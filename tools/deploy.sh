#!/bin/sh
application="blo.re"
application_server="blo.re"
git_server="localhost"
repo_path="/home/dino/webdev/$application"
web_server="lovejoy.kuijstenwebcompany.nl"
web_path="/srv/www/$application_server"

# use a unique timestamp for directory naming
timestamp=$(date +'%Y-%m-%d_%H-%M-%S')

remote() {
    echo "$1: $2"
    ssh -A $1 "$2"
}

# create a temporary directory to checkout the source locally
basename=`basename $0`
remote $git_server "
# export last beta to the web server
cd $repo_path &&
git archive --format=tar --prefix=$timestamp/ master | gzip | ssh $web_server \" tar -C $web_path/releases -zxf - \"

echo \"\nCreate symlinks on $web_server\"
ssh $web_server \"
cd $web_path/releases &&
cd $timestamp &&
rm -rf bootstrap/cache
ln -nfs ../../../shared/cache bootstrap/cache &&
rm -rf storage
ln -nfs ../../shared/storage storage &&
mv .env.production .env &&
cd ../../ &&
ln -nfs releases/$timestamp current
\"
"

echo "done deploying, status: $?"

ssh $web_server -t 'sudo php '$web_path'/current/artisan config:cache && sudo bash ~/scripts/after_deploy_laravel.sh '$application_server
