<?php

if (! function_exists('d')) {
    /**
     * Dump the passed variables and return a string.
     *
     * @param  mixed
     * @return string
     */
    function d($x)
    {
        return json_encode($x, JSON_PRETTY_PRINT);
    }
}

