<?php

namespace App\Models;

use Log;

class Pair extends PairBase
{
    use ModelFunctions;

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|max:12',
    ];
}
