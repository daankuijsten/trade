<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class ProductBase
 * @package App\Models
 */

class NewsItemBase extends Model
{

    public $table = 'news_items';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'news_site_id',
        'url',
        'url_hash',
        'content',
        'site_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'news_site_id' => 'integer',
        'url' => 'string',
        'url_hash' => 'string',
        'content' => 'string',
        'site_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function newsSite()
    {
        return $this->belongsTo(\App\Models\NewsSite::class);
    }

}
