<?php

namespace App\Models;

use Validator;

trait ModelFunctions
{
    /**
     * When this method is called on an object, it performs validation on the object and saves it.
     * When an $attributes array is provided, it is set on the model before validating and saving.
     *
     * @param $errors  An array given by reference and will be set when validation has errors.
     * @param $attributes  An array with attributes to be saved on the model.
     * @param $save  Save the model or don't, default = 1
     * @return bool whether or not the model is valid (and whether it's saved if save is 1)
     */
    public function validSave(&$errors = array(), $attributes = null, $save = 1)
    {
        if ($attributes) {
            $this->fill($attributes);
        }

        $validator = Validator::make($this->getAttributes(), $this->getRules());

        if ($validator->fails()) {
            $errors = array_merge($errors, $validator->errors()->all());
            return false;
        }

        if ($save) {
            return $this->save();
        }

        return true;
    }

    /**
     * A general function to get the rules on a model. Can be either a method
     * "getRulesModel" or a static variable "$rules".
     * The method "getRulesModel" will first be checked, if it not exists,
     * the static $rules variable will be returned.
     *
     * @return array all the rules for this model
     */
    public function getRules()
    {
        if (method_exists(self::class, 'getRulesModel')) {
            return $this->getRulesModel();
        } else {
            return self::$rules;
        }
    }

    /**
     * A general function to set the rules on a model.
     *
     * @return array all the rules for this model
     */
    public function setRules($rules)
    {
        self::$rules = $rules;
        return self::$rules;
    }

    /**
     * Get the names of the related file models. This can be either by having the "file" relationship or an array "nameFileRelations"
     * with names of related file models on it.
     */
    public function getNamesFileRelations() {
        if (isset($this->file)) {
            return ['file'];
        }

        if (isset($this->nameFileRelations)) {
            return $this->nameFileRelations;
        }

        return false;
    }

    /**
     * Check if the attribute is a file attriute.
     */
    public function isFileContentAttribute($attribute) {
        if (empty($attribute)) {
            return false;
        }        

        if ($attribute === 'file_content') {
            return true;
        }        

        $namesFileRelations = $this->getNamesFileRelations();

        // check if attribute end in _content and have a match in file relations
        if ($namesFileRelations && substr($attribute, 0, 12) === 'file_content' && in_array(substr($attribute, 13), $namesFileRelations)) {
            return true;
        }

        return false;
    }
}
