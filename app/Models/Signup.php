<?php

namespace App\Models;

use Log;

class Signup extends SignupBase
{
    use ModelFunctions;

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'email' => 'required|max:191',
        'address' => 'required|max:120',
        'amount_btc' => 'required|numeric',
        'amount_eth' => 'required|numeric',
    ];
}
