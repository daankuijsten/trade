<?php

namespace App\Models;

use Log;
use WGFeed;

class WGKeyword extends WGKeywordBase
{
    use ModelFunctions;

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //'wg_feed_id' => 'required|numeric|exists:wg_feeds.id',
        'keyword' => 'required|max:40',
        'type' => 'required|max:40',
        'value' => 'nullable|max:40',
        'value_normal' => 'nullable|max:40',
        'value_float' => 'nullable|numeric',
    ];

    /**
     * Match the message with a few patterns.
     *
     * Do all the matching in lowercase.
     *
     * @param string $message
     * @return string Comma separated string with matches
     */
    public static function fish($wgFeed) {
        $patterns = [
            'price' => [
                '/(€ *[0-9,.]+)/', // €
                '/([0-9,.]+ *€)/', // €
                '/([0-9,.]+ *euro)/', // euro's
                '/(euro *[0-9,.]+)/', // euro's
            ],
            'm2' => [
                '/([0-9,.]+\s?s?qm)/', // (s)qm
                '/([0-9,.]+\s?m2)/', // m2
                '/([0-9,.]+\s?m²)/', // m2
                '/([0-9,.]+\s?square meters)/',
                '/([0-9,.]+\s?square meter)/',
                '/([0-9,.]+\s?squaremeters)/',
                '/([0-9,.]+\s?squaremeter)/',
            ],
            'bid' => [
                '/(zu vermieten)/', // zu vermieten
                '/(sublet app?artment)/', // sublet apartment
                '/(subletting)/', // sublet apartment
                '/(for rent)/', // sublet apartment
                '/^([ \[\{\(]{1,3}angebot[ \]\}\)]{1,3})/', // [ ANGEBOT ]
                '/^([ \[\{\(]{1,3}offer[ \]\}\)]{1,3})/', // [ offer ]
                '/^(flat in )/', 
                '/^(room in )/', 
                '/^(zimmer in )/', 
                '/^(zimmer zur )/', 
                '/^(full furnished )/', 
                '/^(furnished )/', 
                '/^(double room )/', 
                '/^(triple room )/', 
                '/^(single room )/', 
                '/^(one room )/', 
                '/^(nice room )/', 
                '/^(beautiful )/', 
                '/^(wg zimmer )/', 
                '/^(1 room)/', 
                '/^(2 room)/', 
                '/^(3 room)/', 
                '/^(1 bedroom)/', 
                '/^(2 bedroom)/', 
                '/^(3 bedroom)/', 
                '/^(one bedroom)/', 
                '/^(two bedroom)/', 
                '/^(three bedroom)/', 
                '/^(long term rent)/', 
                '/^(short term rent)/', 
            ],
            'ask' => [
                '/(auf der suche)/', // auf der suche
                '/(ich suche)/', // ich suche
                '/(ich zuche)/', // ich zuche
                '/(suche ich)/', // sublet apartment
                '/(und suche)/', // und suche
                '/(nun suche)/', // nun suche
                '/(wir suchen)/', // ich suche
                '/(suchen wir)/', // ich suche
                '/(looking for a)/', // looking for a
                '/(looking for room)/', // looking for a
                '/(we are looking for)/', // looking for a
                '/(we\'re looking for)/', // looking for a
                '/(I am looking for)/', // looking for a
                '/^([ \[\{\(]{1,3}need wg[ \]\}\)]{1,3})/', // [ need wg ]
                '/^([ \[\{\(]{1,3}request[ \]\}\)]{1,3})/', // [ request ]
                '/^([ \[\{\(]{1,3}notfall[ \]\}\)]{1,3})/', // [ notfall ]
                '/^(suche)/', 
                '/^(search)/', 
                '/^(seeking)/', 
                '/^(wanted)/', 
            ],
        ];

        $keywords = [];

        // Remove the old ones
        WGKeyword::where('wg_feed_id', $wgFeed->id)->delete();

        foreach($patterns as $type => $array) {
            foreach($array as $pattern) {
                preg_match_all($pattern, strtolower($wgFeed->content), $matches);

                if (empty($matches) || empty($matches[1])) {
                    continue;
                }

                $keywords = array_unique($matches[1]);

                if (empty($keywords)) {
                    continue;
                }

                $value = null;

                foreach($keywords as $keyword) {
                    if (!WGKeyword::filter($type, $keyword)) {
                        continue;
                    }


                    $wgKeyword = new WGKeyword([
                        'wg_feed_id' => $wgFeed->id,
                        'keyword' => $keyword,
                        'type' => $type,
                        //'value' => $value,
                    ]);

                    if (in_array($type, ['price', 'm2'])) {
                        $float = WGKeyword::normalize($type, $keyword);
                        // FIXME: check if we only have numbers, else skip
                        $pattern = '/([0-9]+)/'; // only numbers
                        preg_match_all($pattern, $float, $matches);
                        if (empty($matches) || empty($matches[1])) {
                            continue;
                        }

                        $wgKeyword->value_float = $float;
                    } else {
                        $wgKeyword->value_normal = WGKeyword::normalize($type, $keyword);
                    }

                    $wgKeyword->save();
                }
            }
        }

        return $keywords;
    }

    public static function normalize($type, $value)
    { 
        if (empty($value)) {
            return null;
        }

        switch($type) {
            case 'price':
                $match = self::onlyNumbers($value);

                return filter_var($match, FILTER_SANITIZE_NUMBER_INT);
                break;
            case 'm2':
                $value = str_replace(['m2', 'm²'], '', $value);

                $match = self::onlyNumbers($value);

                return filter_var($match, FILTER_SANITIZE_NUMBER_INT);
                break;
        }
    }

    public static function filter($type, $value)
    { 
        switch($type) {
            // When we dont have numbers, reject
            case 'price':
                $pattern = '/([0-9]+)/'; // only numbers
                preg_match_all($pattern, $value, $matches);
                if (empty($matches) || empty($matches[1])) {
                    return false;
                }

                if (strpos($value, '..') !== false) {
                    return false;
                }

                return true;

                break;
        }

        return $value;
    }

    public static function onlyNumbers($value) {
        // When the price has ",00" or ".00", this is behind the comma, so remove it
        // Get the part with numbers and/or , .
        $pattern = '/([0-9,.]+)/'; // only numbers
        preg_match_all($pattern, $value, $matches);
        if (empty($matches) || empty($matches[1])) {
            dd('no match for price on number match', $value, $matches);
            return $value;
        }

        $match = array_pop($matches[1]);

        $pattern = '/([,.]{1}[0-9]{2}$)/'; // only ,00 or .00 at the end of a string
        preg_match_all($pattern, trim($match), $matches);
        if (!empty($matches) && !empty($matches[1]) && strlen($match) > 3) {
            $match = substr($match, 0, -3);
        }

        $pattern = '/([,.]{1}[0-9]{1}$)/'; // only ,0 or .0 at the end of a string
        preg_match_all($pattern, trim($match), $matches);
        if (!empty($matches) && !empty($matches[1]) && strlen($match) > 3) {
            $match = substr($match, 0, -2);
        }

        if (strpos('.', $match) || strpos(',', $match)) {
            dd('still leftover in string', $match);
        }

        return $match;
    }

}
