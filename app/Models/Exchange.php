<?php

namespace App\Models;

use Log;
use Liqui;

class Exchange extends ExchangeBase
{
    use ModelFunctions;

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|max:128',
        'pack_size' => 'required|integer',
        'unit_id' => 'nullable|integer|exists:units,id',
        'file_id' => 'nullable|integer|exists:files,id',
        'item_code' => 'nullable|max:10',
        'brand_id' => 'required|integer|exists:brands,id',
    ];

    public function info() {
        $name = $this->name;
        return $name::info();
    }

    public function trades($pairs) {
        $name = $this->name;

        foreach($pairs as $pair) {
            $names[] = $pair->name;
        }

        $coinPairs = implode('-', $names);

        return $name::trades($coinPairs);
    }

    public function depths($pairs) {
        $name = $this->name;

        foreach($pairs as $pair) {
            $names[] = $pair->name;
        }

        $coinPairs = implode('-', $names);

        return $name::depths($coinPairs);
    }
}
