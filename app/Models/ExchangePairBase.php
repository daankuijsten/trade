<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class ProductBase
 * @package App\Models
 */

class ExchangePairBase extends Model
{

    public $table = 'exchange_pair';
    
    public $fillable = [
        'active',
        'created_on_exchange',
    ];

    public $casts = [
        'active' => 'boolean',
        'created_on_exchange' => 'timestamp',
    ];
}
