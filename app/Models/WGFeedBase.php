<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class ProductBase
 * @package App\Models
 */

class WGFeedBase extends Model
{

    public $table = 'wg_feeds';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'external_id',
        'content',
        'group_title',
        'platform',
        'date',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'external_id' => 'integer',
        'content' => 'string',
        'group_title' => 'string',
        'platform' => 'string',
        'date' => 'datetime',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function wgKeywords()
    {
        return $this->hasMany(\App\Models\WGKeyword::class, 'wg_feed_id');
    }
}
