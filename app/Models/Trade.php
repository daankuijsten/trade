<?php

namespace App\Models;

use Log;
use Liqui;

class Trade extends TradeBase
{
    use ModelFunctions;

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|max:128',
        'pack_size' => 'required|integer',
        'unit_id' => 'nullable|integer|exists:units,id',
        'file_id' => 'nullable|integer|exists:files,id',
        'item_code' => 'nullable|max:10',
        'brand_id' => 'required|integer|exists:brands,id',
    ];

}
