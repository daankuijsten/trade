<?php

namespace App\Models;

use Log;

class WGFeed extends WGFeedBase
{
    use ModelFunctions;

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'external_id' => 'nullable',
        'group_title' => 'nullable|max:256',
        'platform' => 'required|in:fb,wg_gesucht',
        'content' => 'required|max:65000',
        'date' => 'nullable|date',
    ];
}
