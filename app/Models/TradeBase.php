<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class ProductBase
 * @package App\Models
 */

class TradeBase extends Model
{

    public $table = 'trades';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'exchange_id',
        'pair_id',
        'type',
        'price',
        'amount',
        'tid',
        'timestamp',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'exchange_id' => 'integer',
        'pair_id' => 'integer',
        'type' => 'string',
        'price' => 'float',
        'amount' => 'float',
        'tid' => 'integer',
        'timestamp' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function pair()
    {
        return $this->belongsTo(\App\Models\Pair::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function exchange()
    {
        return $this->belongsTo(\App\Models\Exchange::class);
    }
}
