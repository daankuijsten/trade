<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class ProductBase
 * @package App\Models
 */

class WGKeywordBase extends Model
{

    public $table = 'wg_keywords';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'wg_feed_id',
        'keyword',
        'type',
        'value',
        'value_normal',
        'value_float',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'wg_feed_id' => 'integer',
        'keyword' => 'string',
        'type' => 'string',
        'value' => 'string',
        'value_normal' => 'string',
        'value_float' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function wgFeed()
    {
        return $this->belongsTo(\App\Models\WGFeed::class);
    }
}
