<?php

namespace App\Http\Controllers;

use WebApp;
use \App\Models\WGFeed;
use \App\Models\WGKeyword;
use Illuminate\Http\Request;
use Redirect;

class WGController extends Controller
{
    public function overview(Request $request) {

        if (!empty($request->input())) {
            $this->data['wgFeeds'] = $this->handleFilter($request->input());
        } else {
            $this->data['wgFeeds'] = WGFeed::orderBy('date', 'desc')->limit(1000)->get();
        }

        $this->data['filter'] = $request->input();

        return view('wg.overview')->with($this->data);
    }

    public function handleFilter($input)
    {
        $wgFeeds = WGFeed::orderBy('date', 'desc')->limit(1000);

        if (array_key_exists('type', $input) && !empty($input['type'])) {
            $wgFeeds->where(function($q1) use ($input) {
                $q1->whereDoesntHave('wgKeywords', function($q2) use ($input) {
                    $q2->where('type', 'bid');
                    $q2->orWhere('type', 'ask');
                });
                $q1->orWhereHas('wgKeywords', function($q2) use ($input) {
                    $q2->where('type', $input['type']);
                });
            });
        }

        if (array_key_exists('min_price', $input) && !empty($input['min_price'])) {
            $wgFeeds->where(function($q1) use ($input) {
                $q1->whereDoesntHave('wgKeywords', function($q2) use ($input) {
                    $q2->where('type', 'price');
                });
                $q1->orWhereHas('wgKeywords', function($q2) use ($input) {
                    $q2->where('type', 'price')->where('value_float', '>=', $input['min_price']);
                });
            });
        }

        if (array_key_exists('min_m2', $input) && !empty($input['min_m2'])) {
            $wgFeeds->where(function($q1) use ($input) {
                $q1->whereDoesntHave('wgKeywords', function($q2) use ($input) {
                    $q2->where('type', 'm2');
                });
                $q1->orWhereHas('wgKeywords', function($q2) use ($input) {
                    //$q->orWhere(function ($q2) use ($input) {
                        $q2->where('type', 'm2')
                        ->where('value_float', '>=', $input['min_m2']);
                    //});
                });
            });
        }

        if (array_key_exists('string', $input) && !empty($input['string'])) {
            $wgFeeds->where('content', 'like', '%' . $input['string'] . '%');
        }

        return $wgFeeds->get();
    }

    public function connect(Request $request) {
        $fb = new \Facebook\Facebook([
            'app_id' => '148497465809858',
            'app_secret' => '939ba85a42d167d3f1d70881c3236bd3',
            'default_graph_version' => 'v2.10',
            'default_access_token' => 'EAACHDsKzG8IBAP90kJlkFcxZCHXYjD9Qd59dMXf7df0Bcep1uooS3XjZAzuamu4Wz5fq9lfsZC7og6mZAlvxXpZA7APUXZBC7l59Usfw65p1byQZABXFmVu6JlATZASL6Hd3qIdi6ZC1XHR6ZB9BwClnSJ39rTyZBL4TJHxKtpWemKcqAZDZD', // optional
        ]);

        $groupIds = [
            // 21K members
            '1494514567505852', // WG Zimmer Wohnung in Berlin Room Flat Apartment,

            // 6K members
            '1406541762929918', // Wohnung in Berlin

            // 4K members
            '915670238496520', // GeilWohnen Berlin - Wohnung / Room - WG Zimmer / apartment - Immobilien

            // 6K members
            '681027242074146', // Wg zimmer wohnungen berlin room flat apartment rent

            // 31K members
            '547530108636607', // Berliner Wohnungsbörse (Wohnung, Mitbewohner, WG gesucht Berlin)

            // 60K members
            '393237407451209', // Flats in Berlin

            // 30K members
            '316886635183491', // Berlin Housing

            // 6K members
            '248454971973511', // WG, Zimmer, Wohnung, Büro in Berlin gesucht!!!

            // NEWWWW
            //
            // 121K members
            '115932045114177', // WG-Zimmer & Wohnungen Berlin

            // 2K members
            '1298875020165552', // Berlin WG,Wohnung/Haus/Zimmer zu vermieten/Apartments/Rooms/Flats For Rent

            // 38K members
            '292186250836587', // WG, Zimmer, Wohnung, Flat, Room  ❤️  Berlin !!

            // 10K members
            '119191488105936', // Case in condivisione a Berlino (WG gesucht in Berlin)

            // 17K members
            '251856141592447', // Zimmer in Berlin, Zwischenmiete

            // 37K members
            '484268914988517', // Short-term accommodation Berlin: WG, Zwischenmiete, flat-share, Zimmerbörse

            // 60K members
            '314515061903534', // wg zimmer wohnung in berlin room flat apartment rent

            // 5K members
            '100980936759658', // Wohnung/WG in Berlin gesucht

            // 49K members
            '274446799430633', // Wohnung und WG Berlin

            // 5K members
            '212595059151674', // Berlin LONG TERM rooms/flats

            // 12K members
            '1678546859106556', // Flats in Berlin ONLY LONG TERM

        ];

        foreach($groupIds as $groupId) {
            $group = new \StdClass();
            $group->groupId = $groupId;

            try {
                // Get the \Facebook\GraphNodes\GraphUser object for the current user.
                // If you provided a 'default_access_token', the '{access-token}' is optional.
                $responseGroup = $fb->get('/' . $groupId);
                $response = $fb->get('/' . $groupId . '/feed?limit=100');

                $group->name = $responseGroup->getGraphNode()->getProperty('name');

                $messages = $response->getGraphEdge();
                $errors = [];

                do {
                    foreach($messages as $message) {
                        $wgFeed = WGFeed::where('external_id', $message->getProperty('id'))->first();
                        if (!$wgFeed) {
                            $wgFeed = new WGFeed;
                        }

                        $wgFeed->external_id = $message->getProperty('id');
                        $wgFeed->group_title = $responseGroup->getGraphNode()->getProperty('name');
                        $wgFeed->platform = 'fb';
                        $wgFeed->content = $message->getProperty('message');
                        if (!empty($message->getProperty('updated_time'))) {
                            $wgFeed->date = $message->getProperty('updated_time')->format('Y-m-d H:i:s');
                        } else if (!empty($message->getProperty('created_time'))) {
                            $wgFeed->date = $message->getProperty('created_time')->format('Y-m-d H:i:s');
                        } else {
                            dd('no updated or created time found', $message);
                        }

                        if (!$wgFeed->validSave($errors)) {
                            if (empty($wgFeed->content)) {
                                continue;
                            }
                            dd($errors);
                            continue;
                        }

                    }

                    $messages = $fb->next($messages);
                } while (!empty($messages));

            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                // When Graph returns an error
                dd($e, $groupId);
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                dd($e, $groupId);
                dd($e);
                // When validation fails or other local issues
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

        };

        $this->fishKeywords($request);

        dd('done succesfully');
    }

    public function fishKeywords(Request $request, $update = true) {
        $wgFeeds = WGFeed::get();

        foreach($wgFeeds as $wgFeed) {
            if (!$update && !empty($wgFeed->wgKeywords->first()->keywords)) {
                continue;
            }

            $wgKeyword = new WGKeyword();
            $wgKeyword->wg_feed_id = $wgFeed->id;

            $keywords = WGKeyword::fish($wgFeed);
            $wgKeyword->keywords = empty($wgKeyword->keywords) ? $keywords : $wgKeyword->keywords . ',' . $keywords;
            $wgKeyword->validSave();
        }
    }
}
