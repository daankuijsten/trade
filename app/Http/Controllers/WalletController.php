<?php

namespace App\Http\Controllers;

use WebApp;
use \App\Models\Exchange;
use \App\Models\Pair;
use \App\Models\Trade;
use DB;
use Illuminate\Http\Request;
use Redirect;

class WalletController extends Controller
{
    public function redirect(Request $request) {
        return Redirect::to(route('wallet.show'));
    }

    public function show(Request $request) {
        $this->data['haveWallets'] = false;
        if ($request->query()) {
            $this->data['haveWallets'] = true;
            $this->data['url'] = url(route('wallet.show', $request->query()));
        }
        return view('wallet.overview')->with($this->data);
    }
}
