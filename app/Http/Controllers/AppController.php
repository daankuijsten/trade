<?php

namespace App\Http\Controllers;

use WebApp;
use \App\Models\Exchange;
use \App\Models\Pair;
use \App\Models\Signup;
use \App\Models\Trade;
use \App\Models\NewsItem;
use Illuminate\Http\Request;
use DB;
use Redirect;
use MailTemplate;

class AppController extends Controller
{
    public function trade() {
        return view('app.trade')->with($this->data);
    }

    public function getTrades($exchangeCode) {
        $exchange = Exchange::where('code', $exchangeCode)->first();

        $pairs = $exchange->pairs()->where('active', 1)->get();

        $trades = $exchange->trades($pairs);

        foreach($trades as $pairName => $tradeArray) {
            $query = DB::table('trades');
            $pair = Pair::where('name', $pairName)->first();
            $inserts = [];
            $tids = [];

            foreach($tradeArray as $trade) {
                $tids[] = $trade->tid;
            }

            $existingTids = DB::table('trades')
                ->where('exchange_id', $exchange->id)
                ->whereIn('tid', $tids)
                 ->get();

            $existingTids = $existingTids->keyBy('tid')->keys()->toArray();

            foreach($tradeArray as $trade) {
                if (in_array($trade->tid, $existingTids)) {
                    continue;
                }

                $inserts[] = [
                    'exchange_id' => $exchange->id,
                    'pair_id' => $pair->id,
                    'type' => $trade->type,
                    'price' => $trade->price,
                    'amount' => $trade->amount,
                    'tid' => $trade->tid,
                    'timestamp' => date('Y-m-d H:i:s', $trade->timestamp),
                ];
            }

            $query->insert($inserts);
        }
    }

    public function getDepth($exchangeCode) {
        $exchange = Exchange::where('code', $exchangeCode)->first();

        $pairs = $exchange->pairs()->where('active', 1)->get();

        $depths = $exchange->depths($pairs);

        $batchId = md5(microtime() . rand(0,99999));

        foreach($depths as $pairName => $depthsArray) {
            $query = DB::table('depth');
            $pair = Pair::where('name', $pairName)->first();
            $inserts = [];

            foreach($depthsArray->asks as $depth) {
                $inserts[] = [
                    'exchange_id' => $exchange->id,
                    'pair_id' => $pair->id,
                    'batch_id' => $batchId,
                    'type' => 'ask',
                    'price' => $depth[0],
                    'amount' => $depth[1],
                ];
            }

            foreach($depthsArray->bids as $depth) {
                $inserts[] = [
                    'exchange_id' => $exchange->id,
                    'pair_id' => $pair->id,
                    'batch_id' => $batchId,
                    'type' => 'bid',
                    'price' => $depth[0],
                    'amount' => $depth[1],
                ];
            }

            $query->insert($inserts);
        }
    }

    public function getPairs($exchangeCode) {
        $exchange = Exchange::where('code', $exchangeCode)->first();

        $pairs = $exchange->info();
        foreach($pairs->pairs as $name => $object) {
            $pair = Pair::where('name', $name)->first();
            if (!$pair) {
                $pair = new Pair;
                $pair->name = $name;
                $pair->save();
                $exchange->pairs()->save($pair, ['active' => !$object->hidden]);
            } else {
                $exchange->pairs()->updateExistingPivot($pair->id, ['active' => !$object->hidden]);
            }
        }
    }

    public function signup() {
        $this->data['amount_btc'] = 0.074363319;
        $this->data['amount_eth'] = 0.9928347;
        return view('app.signup')->with($this->data);
    }

    public function signupPost(Request $request) {
        $input = $request->all();

        $errors = [];

        try {
            $signup = new Signup;
            $signup->email = $input['email'];
            $signup->address = $input['address'];
            $signup->amount_btc = 0.074363319;
            $signup->amount_eth = 0.9928347;
            $signup->address = $input['address'];
            if (!$signup->validSave($errors)) {
                var_dump($errors);die;
            }

            MailTemplate::mailAdmin('new signup', $signup);

            $this->data['signup'] = $signup;

            return view('app.signup_done')->with($this->data);
        } catch (\Exception $e) {
            dd($e);
        }

        Redirect::back()->withInput();
    }

    public function news() {
        $this->data['newsItems'] = NewsItem::orderBy('created_at', 'desc')->get();

        return view('app.news')->with($this->data);
    }

}
