<?php

namespace App\Http\Controllers;

use WebApp;
use \App\Models\Exchange;
use \App\Models\Pair;
use \App\Models\Trade;
use Trade as TradeService;
use \App\Models\Currency;
use \App\Models\NewsSite;
use \App\Models\NewsItem;
use DB;
use CoinMarketCap;
use Illuminate\Http\Request;
use Scrape;
use MailTemplate;

class ApiController extends Controller
{
    public function getTrades($exchangeCode, $currencyPairName, $startDateTime = null, $endDateTime = null) {
        if (!$startDateTime) {
            $startDateTime = '2017-01-01 00:00:00';
        }

        if (!$endDateTime) {
            $endDateTime = date('Y-m-d H:i:s', time());
        }

        $exchange = Exchange::where('code', $exchangeCode)->first();
        if (!$exchange) { 
            return response()->json(['messages' => ['exchange not found']], 404);
        }

        $pair = Pair::where('name', $currencyPairName)->first();
        if (!$pair) {
            return response()->json(['messages' => ['currency pair not found']], 404);
        }

        $trades = Trade::where('exchange_id', $exchange->id)
            ->where('pair_id', $pair->id)
            ->whereBetween('timestamp', [$startDateTime, $endDateTime])
            ->orderBy('timestamp')
            ->get();

        if (count($trades) === 0) { 
            return response()->json(['messages' => ['no trades found']], 404);
        }



        return response()->json($trades, 200);
    }

    public function getPairs($exchangeCode) {
        $exchange = Exchange::where('code', $exchangeCode)->first();
        if (!$exchange) { 
            return response()->json(['messages' => ['exchange not found']], 404);
        }

        $pairs = $exchange->pairs()->where('active', 1)->get();
        return response()->json($pairs, 200);
    }

    public function getCurrencies() {
        //$currencies = CoinMarketCap::ticker();
        $currencies = Currency::get()->toArray();
        return response()->json($currencies, 200);
    }

    public function getWalletTemplate(Request $request) {
        $input = $request->all();

        $summary = 'you bought ' . $input['c1a'] . ' ' . strtoupper($input['c1']) . ' and sold ' . $input['c2a'] . ' ' . strtoupper($input['c2']);


        if (empty($input['c1'])) {
            return response()->json(['messages' => ['error' => [['Problem with first currency: ' . $summary]]]], 422);
        }

        if (empty($input['c1a'])) {
            return response()->json(['messages' => ['error' => [['Problem with first currency amount: ' . $summary]]]], 422);
        }

        if (empty($input['c2'])) {
            return response()->json(['messages' => ['error' => [['Problem with second currency: ' . $summary]]]], 422);
        }

        if (empty($input['c2a'])) {
            return response()->json(['messages' => ['error' => [['Problem with second currency amount: ' . $summary]]]], 422);
        }

        /*
        if (empty($input['currency3'])) {
            return response()->json(['Please select your first currency'], 422);
        }

        if (empty($input['currency3Amount'])) {
            return response()->json(['Please select your first currency'], 422);
        }
         */

        // FIXME: some symbol's are duplicate, BTM e.g.
        $currency1 = Currency::where('symbol', $input['c1'])->first();

        if (!$currency1) {
            return response()->json(['messages' => ['Code does not exist']], 422);
        }

        $this->data = array_merge($this->data, [
            'currency1' => $currency1->symbol,
            'currency2' => $input['c2'],
            'currency3' => !empty($input['c3']) ? $input['c3'] : null,
            'currency1Amount' => $input['c1a'],
            'currency2Amount' => $input['c2a'],
            'currency3Amount' => $input['c3a'] !== '' ? $input['c3a'] : null,
            'currency1Name' => $currency1->name_id,
        ]);

        $view = view('wallet.show')->with($this->data)->render();


        return response()->json($view, 200);
    }


    public function getNews() {
        $newsSites = NewsSite::where('active', 1)->get();

        foreach($newsSites as $newsSite) {
            $fetchedAt = strtotime($newsSite->fetched_at);
            if ($fetchedAt > (time() - $newsSite->frequency)) {
                continue;
            }

            $newsSite->fetched_at = time();
            $newsSite->save();

            if ($newsSite->id === 3) {
                $this->retrievePairs('pnx');
                Scrape::getPoloniexNews($newsSite->id);
                // stop
                continue;
            }


            // Some news site don't use an url
            if ($newsSite->url) {
                TradeService::createClient();
                $html = TradeService::get($newsSite->url);

                if (!$html) {
                    MailTemplate::mailAdmin('empty html: ' . $newsSite->url);
                    continue;
                }
            }

            if ($newsSite->id === 1) {
                $this->retrievePairs('bhm');
                Scrape::getBithumbNews($newsSite->id, $html);
                continue;
            }

            if ($newsSite->id === 2) {
                Scrape::getBinanceNews($newsSite->id, $html);
                continue;
            }

            if ($newsSite->id === 4) {
                $this->retrievePairs('bfx');
                Scrape::getBitfinexNews($newsSite->id, $html);
                continue;
            }

            if ($newsSite->id === 5) {
                $this->retrievePairs('btx');
                continue;
            }

            if ($newsSite->id === 6) {
                $this->retrievePairs('krk');
                continue;
            }
        }
    }

    public function retrievePairs($exchangeCode) {
        $exchange = Exchange::where('code', $exchangeCode)->first();
        if (!$exchange) {
            throw new \Exception('No exchange found');
        }

        $pairs = $exchange->info();
        if (!$pairs) {
            MailTemplate::mailAdmin($exchange->name . ', no markets in result');
            return false;
        }

        foreach($pairs as $pair) {
            $createdOnExchange = null;
            if ($pair->created_on_exchange) {
                $createdOnExchange = date('Y-m-d H:i:s', $pair->created_on_exchange);
            }

            $pairExisting = Pair::where('name', $pair->name)->first();
            if (!$pairExisting) {

                $pairNew = new Pair;
                $pairNew->name = $pair->name;
                $pairNew->save();
                $exchange->pairs()->save($pairNew, [
                    'active' => $pair->active,
                    'created_on_exchange' => $createdOnExchange,
                ]);

                MailTemplate::mailApiChange($exchange->name  . ' API pair added "' . $pairNew->name . '"', [
                    'pairName' => $pairNew->name,
                    'pairActive' => $pair->active,
                    'pairCreatedOnExchange' => $createdOnExchange,
                ]);
                $parameters = [
                    'to' => '31642510166',
                    'message' => 'blo.re: ' . $exchange->name  . ' API pair added "' . $pairNew->name . '"',
                ];
                TradeService::sendSMS($parameters);

            } else {
                $oldPairPivot = $exchange->pairs()->where('pair_id', $pairExisting->id)->first();
                if (!$oldPairPivot) {
                    // Could be that another exchange already added it. Simply add the pair
                    $exchange->pairs()->save($pairExisting, [
                        'active' => $pair->active,
                        'created_on_exchange' => $createdOnExchange,
                    ]);

                    // TEMP
                    //
                    MailTemplate::mailApiChange($exchange->name  . ' API pair added "' . $pairExisting->name . '"', [
                        'pairName' => $pairExisting->name,
                        'pairActive' => $pair->active,
                        'pairCreatedOnExchange' => $createdOnExchange,
                    ]);
                    continue;
                }

                if (!!$oldPairPivot->pivot->active === $pair->active) {
                    continue;
                }

                $exchange->pairs()->updateExistingPivot($pairExisting->id, ['active' => $pair->active]);

                MailTemplate::mailApiChange($exchange->name  . ' API pair active/unactive "' . $pairExisting->name . '"', [
                    'pairName' => $pairExisting->name,
                    'pairActive' => $pair->active,
                    'pairCreatedOnExchange' => $oldPairPivot->pivot->created_on_exchange,
                ]);

                $parameters = [
                    'to' => '31642510166',
                    'message' => 'blo.re: ' . $exchange->name  . ' API pair active/unactive "' . $pairExisting->name . '"',
                ];
                TradeService::sendSMS($parameters);
            }
        }
    }

}
