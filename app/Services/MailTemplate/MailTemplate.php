<?php

namespace App\Services\MailTemplate;

use App\Models\EmailTemplate;
use App\Models\Setting;
use Mail;
use Validator;

class MailTemplate
{
    protected function safeEmail($email) {
        if (empty($email)) {
            throw new Exception('safeEmail: no email provided');
        }

        /*
        if (config('app.sendMailToClients')) {
            return $email;
        } else {
            return [config('app.emailAdmin')];
        }
        */
        return [config('app.emailAdmin')];
    }

    protected function toString($content) {
        $newContent = '';

        // Content could be an object or an array of objects
        if (is_object($content)) {
            $newContent = d($content);
        }

        // When we have an array, dump every value in it
        if (is_array($content)) {
            foreach($content as $c) {
                $newContent = $newContent . d($c) . "\n\n";
            }
        }

        if (is_string($content)) {
            $newContent = $content;
        }

        return $newContent;
    }

    /**
     * This function mails the admin.
     *
     * @param string $subject
     * @param null $content Array|Object|String Add the content to the body of the email.
     * @param null $exception
     * @return mixed
     */
    public function mailAdmin($subject = '', $content = null, $exception = null)
    {
        $newContent = self::toString($content);

        // Add debug info to content
        $debugInfo = "\n\nDEBUG INFO\n\n";
        if (array_key_exists('class', debug_backtrace()[3])) {
            $debugInfo .= 'Function: ' . debug_backtrace()[3]['class'] . '::' . debug_backtrace()[3]['function'] . "\n\n";
        }

        if ($exception && is_a($exception, \Exception::class)) {
            $debugInfo .= 'Code: ' . $exception->getCode() . "\n";
            $debugInfo .= 'File: ' . $exception->getFile() . "\n";
            $debugInfo .= 'Line: ' . $exception->getLine() . "\n";
            $debugInfo .= 'Message: ' . $exception->getMessage() . "\n\n";
        }

        $content = $newContent . $debugInfo;

        $data = ['content' => $content];

        $sent = Mail::send(['text' => 'mail.admin'], $data, function($m) use ($subject) {
            $m->from(config('app.emailApp'));
            $m->to([config('app.emailAdmin')]);
            $m->subject('blo.re: ' . $subject);
        });

        return true;
    }

    /**
     * This function mails a new item to a user
     *
     * @param string $subject
     * @param null $data
     * @return mixed
     */
    public function mailNewsItem($subject, $data)
    {
        $sent = Mail::send(['html' => 'mail.news_item'], $data, function($m) use ($subject) {
            $m->from(config('app.emailApp'));
            $m->to([config('app.emailAdmin'), 'aurin8181@posteo.de']);
            $m->subject('blo.re: ' . $subject);
        });

        return true;
    }

    /**
     * This function mails a new api change to a user
     *
     * @param string $subject
     * @param null $data
     * @return mixed
     */
    public function mailApiChange($subject, $data)
    {
        $sent = Mail::send(['html' => 'mail.api_change'], $data, function($m) use ($subject) {
            $m->from(config('app.emailApp'));
            $m->to([config('app.emailAdmin'), 'aurin8181@posteo.de']);
            $m->subject('blo.re: ' . $subject);
        });

        return true;
    }
}
