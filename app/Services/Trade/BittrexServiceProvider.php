<?php

namespace App\Services\Trade;

use Illuminate\Support\ServiceProvider;

class BittrexServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('bittrex', function ($app) {
            return new Bittrex($app, config('app.bittrex.url'), config('app.bittrex.key'));
        });

        $this->app->alias('bittrex', 'App\Services\Trade\Bittrex');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['bittrex'];
    }
}
