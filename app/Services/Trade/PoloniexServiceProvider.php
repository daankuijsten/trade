<?php

namespace App\Services\Trade;

use Illuminate\Support\ServiceProvider;

class PoloniexServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('poloniex', function ($app) {
            return new Poloniex($app, config('app.poloniex.url'), config('app.poloniex.key'));
        });

        $this->app->alias('poloniex', 'App\Services\Trade\Poloniex');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['poloniex'];
    }
}
