<?php

namespace App\Services\Trade;

use Trade;
use stdClass;

class Bittrex extends \App\Services\Trade\Trade
{
    public function __construct(\Illuminate\Foundation\Application $app, $apiUrl, $apiKey) {
        $this->headers = [
            'Content-Type' => 'application/json',
            //'Key' => $apiKey,
        ];

        if (empty($this->client)) {
            $this->client = new \GuzzleHttp\Client([
                'base_uri' => $apiUrl, 
                'headers' => $this->headers,
            ]);
        }
    }

    public function info() {
        $markets = $this->get('public/getmarkets');

        $result = json_decode($markets);
        if (empty($result) || !$result->success || empty($result->result)) {
            return false;
        }

        $objects = [];
        foreach($result->result as $pair) {
            $object = new stdClass;
            $object->name = strtolower(str_replace('-', '_', $pair->MarketName));
            $object->active = $pair->IsActive;
            $object->created_on_exchange = strtotime($pair->Created);
            $objects[] = $object;
        }

        return $objects;
    }

}
