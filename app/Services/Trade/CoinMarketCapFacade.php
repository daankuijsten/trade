<?php

namespace App\Services\Trade;

use Illuminate\Support\Facades\Facade;

class CoinMarketCapFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'coin_market_cap';
    }
}
