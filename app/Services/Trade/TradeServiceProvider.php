<?php

namespace App\Services\Trade;

use Illuminate\Support\ServiceProvider;

class TradeServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('trade', function ($app) {
            return new Trade($app);
        });

        $this->app->alias('trade', 'App\Services\Trade\Trade');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['trade'];
    }
}
