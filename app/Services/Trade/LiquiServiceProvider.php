<?php

namespace App\Services\Trade;

use Illuminate\Support\ServiceProvider;

class LiquiServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('liqui', function ($app) {
            return new Liqui($app, config('app.liqui.url'), config('app.liqui.key'));
        });

        $this->app->alias('liqui', 'App\Services\Trade\Liqui');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['liqui'];
    }
}
