<?php

namespace App\Services\Trade;

use Illuminate\Support\ServiceProvider;

class BithumbServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('bithumb', function ($app) {
            return new Bithumb($app, config('app.bithumb.url'), config('app.bithumb.key'));
        });

        $this->app->alias('bithumb', 'App\Services\Trade\Bithumb');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['bithumb'];
    }
}
