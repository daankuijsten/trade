<?php

namespace App\Services\Trade;

use Illuminate\Support\Facades\Facade;

class KrakenFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'kraken';
    }
}
