<?php

namespace App\Services\Trade;

use Trade;
use stdClass;

class Kraken extends \App\Services\Trade\Trade
{
    public function __construct(\Illuminate\Foundation\Application $app, $apiUrl, $apiKey) {
        $this->headers = [
            'Content-Type' => 'application/json',
            //'Key' => $apiKey,
        ];

        if (empty($this->client)) {
            $this->client = new \GuzzleHttp\Client([
                'base_uri' => $apiUrl, 
                'headers' => $this->headers,
            ]);
        }
    }

    public function info() {
        $markets = $this->get('0/public/AssetPairs');

        $result = json_decode($markets);
        if (empty($result) || !empty($result->error) || empty($result->result)) {
            return false;
        }

        $objects = [];
        foreach($result->result as $pair) {
            $object = new stdClass;
            $object->name = strtolower($pair->altname);
            $object->active = true; // FIXME: don't know if pair is deleted
            $object->created_on_exchange = null;
            $objects[] = $object;
        }

        return $objects;
    }

}
