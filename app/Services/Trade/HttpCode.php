<?php

namespace App\Services\Hulk;

use Log;
use MailTemplate;
use Hulk;

class HttpCode
{
    /**
     * Get the messages out of an Exception. The exception is thrown by GuzzleHttp and hold a code, 
     * a body and a response.
     *
     * Do never getMessage() on the Exception, because that holds Guzzle data. 
     * Always check for the body on the response (getBody()) to extract the messages.
     *
     * @param Exception $e
     * @return mixed  Return a messages array or false when something was really wrong.
     */
    public static function getMessages($e) {
        $messages = [];

        if (!self::validException($e)) {
            return $messages;
        }

        // check if the body holds a message
        if (method_exists($e, 'getResponse')) {
            $response = $e->getResponse();
            $body = Hulk::getBodyArray($response);

            if ($body) {
                if (!empty($body->messages) && is_string($body->messages)) {
                    $messages = [$body];
                } else if (!empty($body->messages) && is_object($body->messages)) {
                    $messages = get_object_vars($body->messages);
                } else if (!empty($body->messages)) {
                    $messages = $body->messages;
                }
            }
        } else {
            $messages[] = $e->getMessage();
        }

        $code = $e->getCode();

        switch($code) {
        case 200:
            $messages  = !empty($messages) ? $messages : ['Request done successfully'];
            break;

        case 201:
            $messages  = !empty($messages) ? $messages : ['Item is created'];
            break;

        case 400:
            $messages  = !empty($messages) ? $messages : ['Could not view page'];
            break;

        case 403:
            $messages  = !empty($messages) ? $messages : ['Could not view page, forbidden'];
            break;

        case 404:
            $messages  = !empty($messages) ? $messages : ['Item not found'];
            break;

        case 422:
            $messages  = !empty($messages) ? $messages : ['Could not view page'];
            break;

        case 500:
            $messages  = !empty($messages) ? $messages : ['Error in API, admin is notified'];
            MailTemplate::mailAdmin('Error querying API', [$code, $messages]);
            break;

        default:
            $messages  = !empty($messages) ? $messages : ['Error in the API, admin is notified'];
            break;
        }

        return $messages;
    }

    /**
     * Set a message in an array. Can be:
     * - success
     * - error
     * - info (not used)
     * - warning (not used)
     *
     * @param Exception $e The thrown Exception by GuzzleHttp.
     * @param array $messages  An array with messages where the new message will be added to.
     * @return $messages  The new messages array.
     */
    public static function setMessage($e, $messages) {
        if (!self::validException($e)) {
            return $messages;
        }

        $code = $e->getCode();
        $newMessages = self::getMessages($e);

        // init
        if (empty($messages['success'])) {
            $messages['success'] = [];
        }

        if (empty($messages['error'])) {
            $messages['error'] = [];
        }

        if ($code >= 200 && $code < 300) {
            $messages['success'] = array_merge($messages['success'], $newMessages);
        } else if ($code >= 400) {
            $messages['error'] = array_merge($messages['error'], $newMessages);
        } else {
            Log::notice('Could not determine message with HTTP return code', [$e->getCode()]);
            $messages['error'] = [$e->getMessage()];
            //dd('setMessage exception', $e);
        }

        return $messages;
    }

    /**
     * Check if the Exception has getCode() and getMessage() fuctions
     *
     * @param Exception $e
     * @return boolean
     */
    private static function validException($e) {
        if (!method_exists($e, 'getCode')) {
            Log::error('getCode does not exist on Exception while trying to set message', $e);
            return false;
        }

        if (!method_exists($e, 'getMessage')) {
            Log::error('getMessage does not exist on Exception while trying to set message', $e);
            return false;
        }

        return true;
    }
}
