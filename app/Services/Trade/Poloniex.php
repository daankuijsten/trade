<?php

namespace App\Services\Trade;

use Trade;
use stdClass;

class Poloniex extends \App\Services\Trade\Trade
{
    public function __construct(\Illuminate\Foundation\Application $app, $apiUrl, $apiKey) {
        $this->headers = [
            'Content-Type' => 'application/json',
            //'Key' => $apiKey,
        ];

        if (empty($this->client)) {
            $this->client = new \GuzzleHttp\Client([
                'base_uri' => $apiUrl, 
                'headers' => $this->headers,
            ]);
        }
    }

    public function info() {
        $markets = $this->get('public?command=returnTicker');

        $result = json_decode($markets);
        if (empty($result)) {
            return false;
        }
        
        $result = get_object_vars($result);
        if (empty($result)) {
            return false;
        }

        $objects = [];
        foreach($result as $name => $pair) {
            $object = new stdClass;
            $object->name = strtolower($name);
            $object->active = !$pair->isFrozen;
            $object->created_on_exchange = null;
            $objects[] = $object;
        }

        return $objects;
    }

}
