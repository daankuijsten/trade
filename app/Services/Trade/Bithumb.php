<?php

namespace App\Services\Trade;

use Trade;
use stdClass;

class Bithumb extends \App\Services\Trade\Trade
{
    public function __construct(\Illuminate\Foundation\Application $app, $apiUrl, $apiKey) {
        $this->headers = [
            'Content-Type' => 'application/json',
            //'Key' => $apiKey,
        ];

        if (empty($this->client)) {
            $this->client = new \GuzzleHttp\Client([
                'base_uri' => $apiUrl, 
                'headers' => $this->headers,
            ]);
        }
    }

    public function info() {
        $markets = $this->get('public/ticker/all');

        $result = json_decode($markets);
        if (empty($result) || $result->status !== '0000' || empty($result->data)) {
            return false;
        }

        $objects = [];
        foreach($result->data as $name => $pair) {
            $object = new stdClass;
            $object->name = strtolower($name) . '_krw';
            $object->active = true;
            $object->created_on_exchange = null;
            $objects[] = $object;
        }

        return $objects;
    }

}
