<?php

namespace App\Services\Trade;

use Illuminate\Support\ServiceProvider;

class KrakenServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('kraken', function ($app) {
            return new Kraken($app, config('app.kraken.url'), config('app.kraken.key'));
        });

        $this->app->alias('kraken', 'App\Services\Trade\Kraken');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['kraken'];
    }
}
