<?php

namespace App\Services\Trade;

use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\Psr7\Request;
use App\Services\Trade\HTTPCodes;
use Exception;

class Trade
{
    protected $headers = [];
    protected $baseTime = 1501950831;

    public function request($method, $resource, $body, $headers = [], $callback = null) {
        $response = null;

        /*$body = $this->addNonce($body);
        $this->headers = $this->setApiUser($body);

        if ($body !== null) {
            $body = http_build_query($body);
        }
        */

        try {
            $request = new Request($method, $resource, $this->headers, $body);
            $response = $this->client->send($request);
            $responseBody = (string)$response->getBody();
            $responseBodyArray = self::getBody($response);
            if (!$responseBodyArray) {
                // an empty body is perfectly fine
            }

            if (is_callable($callback)) {
                $callback($response);
            }

            return $responseBodyArray;
        } catch(TransferException $e) {
            throw $e;
        } catch(Exception $e) {
            throw $e;
        }

        if (!$response) {
            $e = new Exception('No response');
            dd('check this exception', $e);
            throw $e;
        }

        dd('response', $response);
    }

    public function get($resource, $body = null, $headers = [], $callback = null) {
        return $this->request('get', $resource, $body, $headers, $callback);
    }

    public function post($resource, $body = null, $headers = [], $callback = null) {
        return $this->request('post', $resource, $body, $headers, $callback);
    }

    public function put($resource, $body = null, $headers = [], $callback = null) {
        return $this->request('put', $resource, $body, $headers, $callback);
    }

    public function patch($resource, $body = null, $headers = [], $callback = null) {
        return $this->request('patch', $resource, $body, $headers, $callback);
    }

    public function delete($resource, $body = null, $headers = [], $callback = null) {
        return $this->request('delete', $resource, $body, $headers, $callback);
    }

    public function head($resource, $body = null, $headers = [], $callback = null) {
        return $this->request('head', $resource, $body, $headers, $callback);
    }

    /**
     * Set the api user key on $this.
     * 
     * This function overwrites the whole auth header.
     *
     * @param string $apiKey
     * @return string  The new auth header.
     */
    public function setApiUser($body) {
        $params = http_build_query($body);
        $authHeader = [
            'Sign' => hash_hmac('sha512', $params, config('app.liqui.secret')),
        ];

        return array_merge($this->headers, $authHeader);
    }

    /**
     * Get the body of a response. 
     *
     * @param Illuminate\Http\Response $response
     * @return mixed  return the body in an array or false when it could not be extracted.
     */
    public function getBody($response) {
        if (empty($response)) {
            return false;
        }

        if (!method_exists($response, 'getBody')) {
            return false;
        }

        $responseBody = (string)$response->getBody();

        return $responseBody;
    }
    

    /**
     * Get the body of a response and convert the json to an array. 
     *
     * @param Illuminate\Http\Response $response
     * @return mixed  return the body in an array or false when it could not be extracted.
     */
    public function getBodyArray($response) {
        if (empty($response)) {
            return false;
        }

        if (!method_exists($response, 'getBody')) {
            return false;
        }

        $responseBody = (string)$response->getBody();

        $responseBodyArray = json_decode($responseBody);

        if (json_last_error() !== JSON_ERROR_NONE) {
            dd('Invalid json return', [$response, $responseBody, $responseBodyArray]);
            return false;
        }

        return $responseBodyArray;
    }
    
    public function addNonce($body) {
        if (!is_array($body)) {
            $body = [$body];
        }
        return array_merge($body, ['nonce' => time() - $this->baseTime]);
    }

    public function createClient() {
        $this->headers = [
            'Content-Type' => 'application/json',
            //'Key' => $apiKey,
        ];

        if (empty($this->client)) {
            $this->client = new \GuzzleHttp\Client([
                'headers' => $this->headers,
            ]);
        }
    }

    public function handleTwitter($tweets) {
        if (!empty($tweets) && is_object($tweets) && !empty($tweets->errors)) {
            throw new Exception($tweets->errors[0]->message);
            return false;
        }
        return true;

    }

    public function sendSMS($params, $backup = false) {
        $token = config('app.smsToken');

        $params['from'] = 'blo.re';

        static $content;

        if($backup == true){
            $url = 'https://api2.smsapi.com/sms.do';
        }else{
            $url = 'https://api.smsapi.com/sms.do';
        }

        $c = curl_init();
        curl_setopt( $c, CURLOPT_URL, $url );
        curl_setopt( $c, CURLOPT_POST, true );
        curl_setopt( $c, CURLOPT_POSTFIELDS, $params );
        curl_setopt( $c, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $c, CURLOPT_HTTPHEADER, array(
           "Authorization: Bearer $token"
        ));

        $content = curl_exec( $c );
        $http_status = curl_getinfo($c, CURLINFO_HTTP_CODE);

        if($http_status != 200 && $backup == false){
            $backup = true;
            self::sendSMS($params, $token, $backup);
        }

        curl_close( $c );
        return $content;
    }
}
