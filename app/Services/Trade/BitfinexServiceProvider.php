<?php

namespace App\Services\Trade;

use Illuminate\Support\ServiceProvider;

class BitfinexServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('bitfinex', function ($app) {
            return new Bitfinex($app, config('app.bitfinex.url'), config('app.bitfinex.key'));
        });

        $this->app->alias('bitfinex', 'App\Services\Trade\Bitfinex');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['bitfinex'];
    }
}
