<?php

namespace App\Services\Trade;

use Trade;

class Binance extends \App\Services\Trade\Trade
{
    public function __construct(\Illuminate\Foundation\Application $app, $apiUrl, $apiKey) {
        $this->headers = [
            'Content-Type' => 'application/json',
            //'Key' => $apiKey,
        ];

        if (empty($this->client)) {
            $this->client = new \GuzzleHttp\Client([
                'base_uri' => $apiUrl, 
                'headers' => $this->headers,
            ]);
        }
    }

    /*
    public function ticker($coinPair) {
        return $this->get('api/3/ticker/' . $coinPair . '?ignore_invalid=1');
    }

    public function balance() {
        return $this->post('tapi', ['method' => 'getInfo']);
    }

    public function info() {
        return $this->get('api/3/info');
    }

    public function trades($coinPair) {
        return $this->get('api/3/trades/'. $coinPair . '?limit=150&ignore_invalid=1');
    }

     */
    public function depths($coinPair) {
        return $this->get('depth?limit=50&symbol=' . strtoupper($coinPair));
    }

}
