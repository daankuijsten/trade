<?php

namespace App\Services\Trade;

use Trade;

class CoinMarketCap extends \App\Services\Trade\Trade
{
    public function __construct(\Illuminate\Foundation\Application $app) {
        $this->headers = [
            'Content-Type' => 'application/json',
            //'Key' => $apiKey,
        ];

        if (empty($this->client)) {
            $this->client = new \GuzzleHttp\Client([
                //'base_uri' => $apiUrl, 
                'headers' => $this->headers,
            ]);
        }
    }

    public function ticker() {
        return $this->get('https://api.coinmarketcap.com/v1/ticker/');
    }
}
