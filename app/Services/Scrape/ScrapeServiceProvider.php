<?php

namespace App\Services\Scrape;

use Illuminate\Support\ServiceProvider;

class ScrapeServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('scrape', function ($app) {
            return new Scrape($app);
        });

        $this->app->alias('scrape', 'App\Services\Scrape\Scrape');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['scrape'];
    }
}
