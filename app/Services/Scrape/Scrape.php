<?php

namespace App\Services\Scrape;

use MailTemplate;
use \App\Models\NewsItem;
use \Abraham\TwitterOAuth\TwitterOAuth;
use Trade;


class Scrape
{
    public function getBithumbNews($newsSiteId, $html) {
        $pattern = '#' . preg_quote('<h3 class="entry-title"') . '(.+?)</h3>#si';
        preg_match_all($pattern, $html, $matches);

        if (empty($matches) || empty($matches[1])) {
            MailTemplate::mailAdmin('bithumb, no matching items');
            return false;
        }

        foreach($matches[1] as $match) {
            $pattern = '#' . preg_quote('href="') . '(.+)' . preg_quote('"') . '#i';
            preg_match($pattern, $match, $matches2);

            if (empty($matches2) || empty($matches2[1])) {
                MailTemplate::mailAdmin('bithumb, no matching items, url');
                return false;
            }

            $url = $matches2[1];

            $newsItemExists = NewsItem::where('url_hash', hash('md5', $url))->first();
            if ($newsItemExists) {
                continue;
            }

            $newsItem = new NewsItem;
            $newsItem->news_site_id = $newsSiteId;
            $newsItem->url = $url;
            $newsItem->url_hash = hash('md5', $newsItem->url);
            $newsItem->content = null;
            $newsItem->save();

            MailTemplate::mailNewsItem('bithumb new item', ['url' => $newsItem->url]);
            $parameters = [
                'to' => '31642510166',
                'message' => 'blo.re: bithumb new item',
            ];
            Trade::sendSMS($parameters);

            //$title = TradeService::get('https://translate.yandex.net/api/v1/tr.json/detect?sid=59c2eca4.59a19f3a.9ba65066&srv=tr-text&text=' . urlencode($title) . '&hint=ko%2Cen&options=1');
            //dd($title);
        }

        return true;

    }


    public function getBinanceNews($newsSiteId, $html) {
        $pattern = '#' . preg_quote('class="article-list-item') . '.*?' . 'href="' . '(.+?)' . preg_quote('"') . '#si';
        preg_match_all($pattern, $html, $matches);

        if (empty($matches) || empty($matches[1])) {
            MailTemplate::mailAdmin('binance, no matching items');
            return false;
        }

        foreach($matches[1] as $match) {
            /*
            $pattern = '#' . preg_quote('href="') . '(.+)' . preg_quote('"') . '#i';
            preg_match($pattern, $match, $matches2);

            if (empty($matches2) || empty($matches2[1])) {
                MailTemplate::mailAdmin('bithumb, no matching items, url');
                return false;
            }

             */
            $blacklist = [
                'https://www.binance.com/',
                'Daily-News',
            ];

            foreach($blacklist as $blacklist) {
                if (strstr($match, $blacklist)) {
                    continue 2;
                }
            }

            $url = 'https://binance.zendesk.com' . $match;

            $newsItemExists = NewsItem::where('url_hash', hash('md5', $url))->first();
            if ($newsItemExists) {
                continue;
            }

            $newsItem = new NewsItem;
            $newsItem->news_site_id = $newsSiteId;
            $newsItem->url = $url;
            $newsItem->url_hash = hash('md5', $newsItem->url);
            $newsItem->content = null;
            $newsItem->save();

            MailTemplate::mailNewsItem('binance new item', ['url' => $newsItem->url]);
            $parameters = [
                'to' => '31642510166',
                'message' => 'blo.re: binance new item',
            ];
            Trade::sendSMS($parameters);

            //$title = TradeService::get('https://translate.yandex.net/api/v1/tr.json/detect?sid=59c2eca4.59a19f3a.9ba65066&srv=tr-text&text=' . urlencode($title) . '&hint=ko%2Cen&options=1');
            //dd($title);
        }

        return true;

    }

    public function getPoloniexNews($newsSiteId) {
        try {
            $connection = new TwitterOAuth(config('app.twitter.key'), config('app.twitter.secret'), config('app.twitter.accessToken'), config('app.twitter.accessTokenSecret'));
            $connection->setTimeouts(10, 15);

            $tweets = $connection->get('statuses/user_timeline', ['screen_name' => 'poloniex']);

            Trade::handleTwitter($tweets);

            foreach($tweets as $tweet) {

                $id = $tweet->id;

                // In this case we use the twitter id instead of the url_hash
                $newsItemExists = NewsItem::where('url_hash', $id)->first();
                if ($newsItemExists) {
                    continue;
                }

                $url = 'https://twitter.com/statuses/' . $id;

                $newsItem = new NewsItem;
                $newsItem->news_site_id = $newsSiteId;
                $newsItem->url = $url;
                $newsItem->url_hash = $id;
                $newsItem->content = $tweet->text;
                $newsItem->save();

                MailTemplate::mailNewsItem('poloniex new item', ['url' => $newsItem->url]);
            $parameters = [
                'to' => '31642510166',
                'message' => 'blo.re: poloniex new item',
            ];
            Trade::sendSMS($parameters);
            }
        } catch(\Exception $e) {
            MailTemplate::mailAdmin('twitter poloniex: connecting to twitter went wrong: ' . $e->getMessage());
        }

        return false;
    } 

    public function getBitfinexNews($newsSiteId, $html) {
        // We also catch the title, but we don't need it
        $pattern = '#change-log-section.*?href=' . preg_quote('"') . '(.+?)' . preg_quote('"') . preg_quote(' class="ajax" data-remote="true">') . '(.+?)' . preg_quote('</a>') . '#si';
        preg_match_all($pattern, $html, $matches);

        if (empty($matches) || empty($matches[1])) {
            MailTemplate::mailAdmin('bitfinex, no matching items');
            return false;
        }

        foreach($matches[1] as $match) {
            $url = 'https://www.bitfinex.com' . $match;

            $newsItemExists = NewsItem::where('url_hash', hash('md5', $url))->first();
            if ($newsItemExists) {
                continue;
            }

            $newsItem = new NewsItem;
            $newsItem->news_site_id = $newsSiteId;
            $newsItem->url = $url;
            $newsItem->url_hash = hash('md5', $newsItem->url);
            $newsItem->content = null;
            $newsItem->save();

            MailTemplate::mailNewsItem('bitfinex new item', ['url' => $newsItem->url]);
            $parameters = [
                'to' => '31642510166',
                'message' => 'blo.re: bitfinex new item',
            ];
            Trade::sendSMS($parameters);
        }

        return true;

    }
}
