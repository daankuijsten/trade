<?php

namespace App\Services\Scrape;

use Illuminate\Support\Facades\Facade;

class ScrapeFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'scrape';
    }
}
