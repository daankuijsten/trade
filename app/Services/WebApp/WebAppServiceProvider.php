<?php

namespace App\Services\WebApp;

use Illuminate\Support\ServiceProvider;

class WebAppServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('web_app', function ($app) {
            return new WebApp($app);
        });

        $this->app->alias('web_app', 'App\Services\WebApp\WebApp');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['web_app'];
    }
}
