<?php

namespace App\Services\WebApp;

use Illuminate\Support\Facades\Facade;

class WebAppFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'web_app';
    }
}