<?php

namespace App\Services\WebApp;

use Session;

class WebApp
{
    public function setFlashMessages($messages) {
        $sessionMessages = Session::get('messages');
        if (!$sessionMessages || !is_array($sessionMessages)) {
            $sessionMessages = [];
        }

        $allMessages = array_merge($sessionMessages, $messages);
        Session::flash('messages', $allMessages);
        return $allMessages;
    }

    public function getAllMessages($messages) {
        $sessionMessages = Session::get('messages');
        if (!$sessionMessages || !is_array($sessionMessages)) {
            $sessionMessages = [];
        }

        if (!is_array($messages)) {
            $messages = [];
        }

        return array_merge($sessionMessages, $messages);
    }

    public function onlyTradeablePairs($pairs) {
        $tradeablePairs = [];
        foreach($pairs as $pair) {
            foreach(config('app.tradeablePairs') as $tradeablePair) {
                if (strstr(strtolower($pair), strtolower($tradeablePair))) {
                    $tradeablePairs[] = $pair;
                }
            }
        }

        return $tradeablePairs;
    }

}
