<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    */

    'name' => env('APP_NAME', 'Laravel'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'Europe/Amsterdam',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
    */

    'log' => env('APP_LOG', 'single'),

    'log_level' => env('APP_LOG_LEVEL', 'debug'),

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,

        /*
         * Package Service Providers...
         */
        Laravel\Tinker\TinkerServiceProvider::class,

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

        App\Services\MailTemplate\MailTemplateServiceProvider::class,
        App\Services\Trade\TradeServiceProvider::class,
        App\Services\Scrape\ScrapeServiceProvider::class,
        App\Services\Trade\LiquiServiceProvider::class,
        App\Services\Trade\CoinMarketCapServiceProvider::class,
        App\Services\WebApp\WebAppServiceProvider::class,
        App\Services\Trade\BittrexServiceProvider::class,
        App\Services\Trade\KrakenServiceProvider::class,
        App\Services\Trade\PoloniexServiceProvider::class,
        App\Services\Trade\BithumbServiceProvider::class,
        App\Services\Trade\BitfinexServiceProvider::class,
        App\Services\Trade\BinanceServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Broadcast' => Illuminate\Support\Facades\Broadcast::class,
        'Bus' => Illuminate\Support\Facades\Bus::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,

        'MailTemplate' => App\Services\MailTemplate\MailTemplateFacade::class,
        'Trade' => App\Services\Trade\TradeFacade::class,
        'Scrape' => App\Services\Scrape\ScrapeFacade::class,
        'Liqui' => App\Services\Trade\LiquiFacade::class,
        'CoinMarketCap' => App\Services\Trade\CoinMarketCapFacade::class,
        'WebApp' => App\Services\WebApp\WebAppFacade::class,
        'Bittrex' => App\Services\Trade\BittrexFacade::class,
        'Kraken' => App\Services\Trade\KrakenFacade::class,
        'Poloniex' => App\Services\Trade\PoloniexFacade::class,
        'Bithumb' => App\Services\Trade\BithumbFacade::class,
        'Bitfinex' => App\Services\Trade\BitfinexFacade::class,
        'Binance' => App\Services\Trade\BinanceFacade::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Variables
    |--------------------------------------------------------------------------
     */

    'liqui' => [
        'url' => env('LIQUI_URL'),
        'secret' => env('LIQUI_SECRET'),
        'key' => env('LIQUI_KEY'),
    ],

    'bittrex' => [
        'url' => 'https://bittrex.com/api/v1.1/',
        'secret' => null,
        'key' => null,
    ],

    'kraken' => [
        'url' => 'https://api.kraken.com/',
        'secret' => null,
        'key' => null,
    ],

    'poloniex' => [
        'url' => 'https://poloniex.com/',
        'secret' => null,
        'key' => null,
    ],

    'bithumb' => [
        'url' => 'https://api.bithumb.com/',
        'secret' => null,
        'key' => null,
    ],

    'bitfinex' => [
        'url' => 'https://api.bitfinex.com/v1/',
        'secret' => null,
        'key' => null,
    ],

    'binance' => [
        'url' => 'https://api.binance.com/api/v1/',
        'secret' => 'iWxPTPsNeMuWHKlEXJ1QDcJwxoFj1wSidrEcwnoXOCXrEEirmS70gtJUUFGoLBDj',
        'key' => '2fu0UK1ucsRLL7ENWhFlkPvw5sX9V0qTOI1dpn8TDhHrJhwFoEPikiWZZS1NKjSV',
    ],

    'twitter' => [
        'secret' => '66AT4C41XInWbWUjLKKtDAOqBP2zd7YVMZQf69vLJTAI3Jp9oq',
        'key' => 'wVq5TCKdwZSfBrWbzdu4lpHqh',
        'accessToken' => '903672643185991682-MOiVWwRWfqLZIjcqR52krAp0AzUKmmy',
        'accessTokenSecret' => 'SESvmXzDuXbTyaqdctCOJuBZeOAAP9mcDddG5xYsMDE8C',
    ],

    'tradeablePairs' => [
        'ETH',
    ],

    'emailAdmin' => 'support@blo.re',
    'emailApp' => 'app@blo.re',

    'smsToken' => env('SMS_TOKEN'),

];
