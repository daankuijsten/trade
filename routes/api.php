<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['api']], function() {
    Route::get('/get-trades/{exchangeCode}/{currencyPairName}/{startDateTime?}/{endDateTime?}', 'ApiController@getTrades')->name('api.getTrades');

    Route::get('/get-pairs/{exchangeCode}', 'ApiController@getPairs')->name('api.getPairs');

    // Wallet
    Route::get('/get-currencies', 'ApiController@getCurrencies')->name('api.getCurrencies');
    Route::get('/get-wallet-template', 'ApiController@getWalletTemplate')->name('api.getWalletTemplate');

    Route::get('/get-news', 'ApiController@getNews')->name('api.getNews');

    // Retrieve stuff from exhanges
    Route::get('/retrieve-pairs/{exchangeCode}', 'ApiController@retrievePairs')->name('api.retrievePairs');


});
