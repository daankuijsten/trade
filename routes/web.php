<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => []], function() {
    Route::get('/', 'AppController@signup')->name('app.signup');
    Route::post('/', 'AppController@signupPost')->name('app.signupPost');

    //Route::get('/email-sent', 'AppController@signupDone')->name('app.signupDone');
    /*
    Route::get('/trade', 'AppController@trade')->name('app.trade');

    Route::get('/get-pairs/{exchangeCode}', 'AppController@getPairs')->name('app.getPairs');
    Route::get('/get-depth/{exchangeCode}', 'AppController@getDepth')->name('app.getDepth');
    Route::get('/get-trades/{exchangeCode}', 'AppController@getTrades')->name('app.getTrades');
     */

    //Route::get('/news', 'AppController@news')->name('app.news');

    /**
     * Wallet
     */
    Route::get('/wallets', 'WalletController@show')->name('wallet.show');
    Route::post('/wallets', 'WalletController@add')->name('wallet.add');

    Route::get('/bot', 'BotController@overview')->name('bot.overview');

    Route::get('/wg', 'WGController@overview')->name('wg.overview');
    Route::get('/wg/refresh', 'WGController@connect')->name('wg.connect');
    Route::get('/wg/fish-keywords', 'WGController@fishKeywords')->name('wg.fishKeywords');
});
