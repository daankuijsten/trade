<div id="messages">
    @foreach (['success', 'error', 'warning', 'info'] as $type)
        @php($messagesAll = WebApp::getAllMessages($messages))

        @if (!empty($messagesAll[$type]))
            @forelse($messagesAll[$type] as $message)
                @if (is_array($message))
                    @php($message = $message[0])
                @endif
                <p class="alert alert-{{ $type }}">{{ $message }}<span class="close ion-android-close"></span></p>
            @empty
            @endforelse
        @endif
    @endforeach
</div>
