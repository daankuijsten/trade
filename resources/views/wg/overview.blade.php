@extends ('wg/index')

@section('content')
    <h1>FB Groups</h1>

    <p>Results: {{ $wgFeeds->count() }}</p>

    <div class="filter">
        <form method="get">
            <select name="type">
                <option value="">-</option>
                <option {{ !empty($filter['type']) && $filter['type'] === 'bid' ? 'selected' : '' }} value="bid">Bid</option>
                <option {{ !empty($filter['type']) && $filter['type'] === 'ask' ? 'selected' : '' }} value="ask">Ask</option>
            </select>

            <input type="text" value="{{ !empty($filter['min_price']) ? $filter['min_price'] : '' }}" placeholder="Minimal price" name="min_price" />
            <input type="text" value="{{ !empty($filter['min_m2']) ? $filter['min_m2'] : '' }}" placeholder="Minimal m2" name="min_m2" />
            <input type="text" value="{{ !empty($filter['string']) ? $filter['string'] : '' }}" placeholder="keyword" name="string" />
        
            <input type="submit" />
        </form>
    </div>

    <div class="groups">
            <table>
                <tr>
                    <th class="date">Date</th>
                    <th class="type">Type</th>
                    <th class="message">Message</th>
                    <th class="keywords">Keywords</th>
                    <th class="price">Price</th>
                    <th class="m2">m2</th>
                </tr>

                @foreach($wgFeeds as $wgFeed)
                    @include('wg.feed',['wgFeed' => $wgFeed])
                @endforeach
            </table>
    </div>
@endsection
