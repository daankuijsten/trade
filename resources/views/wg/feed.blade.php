<tr>
    <td>{{ date('Y-m-d H:i', strtotime($wgFeed->date)) }}</td>
    <td>{{ $wgFeed->wgKeywords->where('type', 'ask')->count() ? 'ask' : '' }} {{ $wgFeed->wgKeywords->where('type', 'bid')->count() ? 'bid' : '' }}</td>
    <td>{{ $wgFeed->content }}</td>
    <td>{{ implode("\n", $wgFeed->wgKeywords->pluck('keyword')->all()) }}</td>
    <td>{{ $wgFeed->wgKeywords->where('type', 'price')->count() ? $wgFeed->wgKeywords->where('type', 'price')->first()->value_float : '' }}</td>
    <td>{{ $wgFeed->wgKeywords->where('type', 'm2')->count() ? $wgFeed->wgKeywords->where('type', 'm2')->first()->value_float : '' }}</td>
</tr>
<tr>
    <td colspan="6"><a target="_blank" href="https://www.facebook.com/groups/{{ $wgFeed->external_id }}">{{ $wgFeed->group_title }}</a></td>
