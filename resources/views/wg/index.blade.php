<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @if (!empty($headerCss))
        @foreach ($headerCss as $headerCssScript)
            <link href="{{ asset ($headerCssScript) }}" rel="stylesheet" type="text/css" />
        @endforeach
    @endif

    @if (!empty($headerJs))
        @foreach ($headerJs as $headerJsScript)
            <script src="{{ asset ($headerJsScript) }}" type="text/javascript"></script>
        @endforeach
    @endif


    <script src="{{ asset ("/js/wg.js") }}" type="text/javascript"></script>
    <link href="{{ asset("/css/wg.css")}}" rel="stylesheet" type="text/css" />
</head>
<body>
    <div id="loading-parent">
        <div class="loading-icon"></div>
    </div>

    <div class="wrapper">

        @include('header')

        <div class="content-wrapper">
            <section class="content">

                @include('messages')

                @yield('content')

            </section>
        </div>

        @include('footer')
    </div>

    @if (!empty($footerJs))
        @foreach ($footerJs as $footerJsScript)
            <script src="{{ asset ($footerJsScript) }}" type="text/javascript"></script>
        @endforeach
    @endif

</body>
</html>
