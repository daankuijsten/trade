@include('app._header')

	<!-- Section: intro -->
    <section id="intro" class="intro">
		<div class="intro-content">
			<div class="container">
				
				<div class="row">
					<div class="col-sm-6 col-md-6 col-lg-6">
					<div class="wow fadeInDown" data-wow-offset="0" data-wow-delay="0.1s">
						<img src="img/intro2.png" class="img-responsive" alt="" />

					</div>
					</div>
					<div class="col-sm-6 col-md-6 col-lg-6 slogan">
						<div class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
						<h1>Almost done, pay to activate</h1>
						<p>
						You can either pay with BTC or ETH.
						</p>
						<p>
                        1. BTC: Pay the amount of {{ $signup->amount_btc }} to our BTC wallet address: 3CvpaoZ4Y79YZrarwhFUgpz9Ds7etCQ9XL 
<br><br>or<br><br>

                        2. ETH: Pay the amount of {{ $signup->amount_eth }} to our ETH wallet address: 0xC41A187a4b05EFABc14C0324251282B795DC0e58
						</p>
						<p>
						After confirmation of your payment you will receive an email that the news feed is activated. {{-- These instructions have also been sent to your email address. --}}
						</p>
                    </div>
				</div>		
			</div>
		</div>		
    </section>

	<footer>
	

		<div class="sub-footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-6 col-lg-6">
	
				
					<div class="wow fadeInLeft" data-wow-delay="0.1s">
					<div class="text-left">
					<p>&copy;Copyright 2017 - Blo.re All rights reserved.</p>
					</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-6">
					<div class="wow fadeInRight" data-wow-delay="0.1s">
					</div>
				</div>
			</div>	
		</div>
		</div>
	</footer>

</div>
@include('app._footer')
