<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Blo.re: profit from newly listed cryptocurrencies on exchanges.</title>
	
    <!-- css -->
    <link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ url('css/nivo-lightbox.css') }}" rel="stylesheet" />
	<link href="{{ url('css/nivo-lightbox-theme/default/default.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ url('css/animate.css') }}" rel="stylesheet" />
    <link href="{{ url('css/style.css') }}" rel="stylesheet">

	<!-- template skin -->
	<link id="t-colors" href="{{ url('css/color/default.css') }}" rel="stylesheet">

    @if (App::environment('production'))

<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  // tracker methods like "setCustomDimension" should be called before "trackPageView"
  _paq.push(["setDocumentTitle", document.domain + "/" + document.title]);
  _paq.push(["setCookieDomain", "*.blo.re"]);
  _paq.push(["setDomains", ["*.blo.re"]]);
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//a.kuijstenwebcompany.nl/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', '2']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img src="//a.kuijstenwebcompany.nl/piwik.php?idsite=2&rec=1" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->

@endif

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">
<div id="wrapper">
	
    <nav class="navbar navbar-custom" role="navigation">
        <div class="container navigation">
		
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="{{ url(route('app.signup')) }}">
                    <img src="{{ url('img/logo-medium.png') }}" alt="" />
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
			  <ul class="nav navbar-nav">
				<li class=""><a href="#intro">Signup</a></li>
                <li class="earn-back"><a href="#earn-your-investment-back">Earn you investment back<span class="label label-success"><i class="fa fa-arrow-up "></i>192%</span></a></li>
				<li class=""><a href="#our-service">Our service</a></li>
				<li class=""><a href="#contact">Contact</a></li>
			  </ul>
            </div>
            <!-- /.navbar-collapse -->

        </div>
        <!-- /.container -->
    </nav>
	
