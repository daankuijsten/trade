<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>

	<!-- Core JavaScript Files -->
    <script src="{{ url('js/jquery.min.js') }}"></script>	 
    <script src="{{ url('js/bootstrap.min.js') }}"></script>
    <script src="{{ url('js/jquery.easing.min.js') }}"></script>
	<script src="{{ url('js/wow.min.js') }}"></script>
	<script src="{{ url('js/jquery.scrollTo.js') }}"></script>
	<script src="{{ url('js/nivo-lightbox.min.js') }}"></script>
    <script src="{{ url('js/custom.js') }}"></script>


</body>

</html>
