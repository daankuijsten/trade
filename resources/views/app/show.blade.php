@extends ('index')

@section('content')
    <table>
        <tr>
            <th>Pairs</th>
        </tr>
        @foreach($pairs as $pair)
            <tr>
                <td>{{ strtoupper(str_replace('_', ' ', $pair)) }}</td>
            </tr>
        @endforeach
    </table>
@endsection
