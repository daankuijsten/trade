@extends ('index')

@section('content')
    <table>
        <tr>
            <th>Item</th>
            <th>Date</th>
        </tr>
        @foreach($newsItems as $newsItem)
            <tr>
                <td><a href="{{ $newsItem->url }}">{{ $newsItem->url }}</a></td>
                <td>{{ date('Y-m-d H:i:s', strtotime($newsItem->created_at)) }}</a></td>
            </tr>
        @endforeach
    </table>
@endsection
