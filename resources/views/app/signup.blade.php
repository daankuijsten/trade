@include('app._header')
	<!-- Section: intro -->
    <section id="intro" class="intro">
		<div class="intro-content">
			<div class="container">
				
				<div class="row">
					<div class="col-sm-5 col-md-5 col-lg-5">
					<div class="wow fadeInDown" data-wow-offset="0" data-wow-delay="0.1s">
						<img src="img/intro2.png" class="img-responsive" alt="" />
                        <p class="image-underline"><small>When a coin hits a new exchange, prices rise up to 192%</small></p>
					</div>
					</div>
					<div class="col-sm-7 col-md-7 col-lg-7 slogan">
						<div class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
						<h1>Gain Up To 192% Profit Because Of Newly Listed Cryptocurrencies On Exchanges</h1>
						<p>
						Receive news of newly listed cryptocurrencies immediately from all the big exchanges. Register your e-mail address and pay with ETH or BTC. 
						</p>
						<p>
						</p>

                    </div>

                    <form class="form" method="post"> 
                        {!! csrf_field() !!}
                        <input name="email" data-wow-duration="2s" data-wow-delay="0.2s" type="email" required placeholder="Your e-mail address" class="email fadeInLeft wow" />
                        <br />
                        <input name="address" data-wow-duration="2s" data-wow-delay="0.2s" type="text" required placeholder="Your ETH or BTC wallet address where you will be paying from" class="address fadeInLeft wow" />
                        <br />
						<div class="buttons">
                            <input type="submit" value="Sign up" class="btn btn-success btn-sm wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.2s" />
                            <p class="wow fadeInLeft smally" data-wow-duration="2s" data-wow-delay="0.2s"><small>(You can either pay {{ $amount_btc }} BTC or {{ $amount_eth }} ETH)</small></p>
                        </div>
                    </form>
						
					</div>					
				</div>		
			</div>
		</div>		
    </section>
	
	<!-- /Section: intro -->
	<div class="divider-short"></div>
	
    <section id="earn-your-investment-back" class="home-section">
	
		<div class="container">
			<div class="row text-center heading">
                <h2>See how quickly you earn your investment back</h2>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<div class="wow fadeInRight" data-wow-delay="0.3s">
						<div class="features">							
						</div>
						<div class="features">							
							<i class="fa fa-arrow-circle-up fa-2x float-left"></i>
							<h5><strong class="percentage">192%</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Metaverse ETP got listed on Bitfinex</h5>
							<p>
							After Metaverse ETP got listed on Bitfinex the price rose from $1.00 up to $2.92.
							</p>
						</div>
						<div class="features">							
							<i class="fa fa-arrow-circle-up fa-2x float-left"></i>
							<h5><strong class="percentage">129%</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gas got listed on Poloniex</h5>
							<p>
							After Gas got listed on Poloniex the price rose from $7.03 up to $16.13.
							</p>
                        </div>
						<div class="features">							
							<i class="fa fa-arrow-circle-up fa-2x float-left"></i>
							<h5><strong class="percentage">79%</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monero got listed on Bithumb</h5>
							<p>
							After Monero got listed on Bithumb the price rose from $52.97 up to $95.08.
							</p>
						</div>
						<div class="features">							
							<i class="fa fa-arrow-circle-up fa-2x float-left"></i>
							<h5><strong class="percentage">54%</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Neo got listed on Bitfinex</h5>
							<p>
							After Neo got listed on Bitfinex the price rose from $21.11 up to $32.65.
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="wow fadeInLeft" data-wow-delay="0.2s">
						<img src="img/img-1.png" alt="" class="img-responsive" />
					</div>
				</div>
			</div>
		</div>

	</section>
	<!-- /Section: content -->
	
	<div class="divider-short"></div>
	
    <section id="our-service" class="home-section">
	
		<div class="container">
			<div class="row text-center heading">
				<h3>When A Cryptocurrency Gets Listed On A Big Exchange The Price Rises</h3>
			</div>
			
			
			<div class="row">
				<div class="col-md-6">
					<div class="wow fadeInLeft" data-wow-delay="0.2s">
						<p>
						Receive news of newly listed cryptocurrencies immediately from all the big exchanges. We keep track of the official news channels and translate the news items. You make the money.
						</p>
						<div class="divider-short marginbot-30 margintop-30"></div>
						<div class="features">							
							<i class="fa fa-bell fa-2x circled bg-skin float-left"></i>
							<h5>We notify you immediately. Speed is key.</h5>
						</div>
						<div class="features">							
							<i class="fa fa-bookmark fa-2x circled bg-skin float-left"></i>
							<h5>We keep track of the official news channels.</h5>
						</div>
						<div class="features">							
							<i class="fa fa-globe fa-2x circled bg-skin float-left"></i>
							<h5>We translate news if necessary.</h5>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="wow fadeInRight" data-wow-delay="0.3s">
						<img src="img/img-2.png" alt="" class="img-responsive" />
					</div>
				</div>

			</div>
		</div>

	</section>
	<!-- /Section: content -->
	
	<div class="divider-short"></div>
{{--
	<div class="divider-short"></div>
    <section id="works" class="home-section text-center">
		<div class="container">
			<div class="row text-center heading">
				<h3>Screenshots</h3>
			</div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12" >

                    <div class="row gallery-item">
                        <div class="col-md-3">
							<a href="img/works/1.jpg" title="This is an image title" data-lightbox-gallery="gallery1" data-lightbox-hidpi="img/works/1@2x.jpg">
								<img src="img/works/1.jpg" class="img-responsive" alt="img">
							</a>
						</div>
						<div class="col-md-3">
							<a href="img/works/2.jpg" title="This is an image title" data-lightbox-gallery="gallery1" data-lightbox-hidpi="img/works/1@2x.jpg">
								<img src="img/works/2.jpg" class="img-responsive" alt="img">
							</a>
						</div>
						<div class="col-md-3">
							<a href="img/works/3.jpg" title="This is an image title" data-lightbox-gallery="gallery1" data-lightbox-hidpi="img/works/1@2x.jpg">
								<img src="img/works/3.jpg" class="img-responsive" alt="img">
							</a>
						</div>
						<div class="col-md-3">
							<a href="img/works/4.jpg" title="This is an image title" data-lightbox-gallery="gallery1" data-lightbox-hidpi="img/works/1@2x.jpg">
								<img src="img/works/4.jpg" class="img-responsive" alt="img">
							</a>
						</div>
					</div>
	
                </div>
            </div>	
		</div>
	</section>

--}}

    <section id="intro" class="intro">
		<div class="intro-content">
			<div class="container">
				
				<div class="row">
					<div class="col-sm-6 col-md-6 col-lg-6">
					<div class="wow fadeInDown" data-wow-offset="0" data-wow-delay="0.1s">
						<img src="img/intro2.png" class="img-responsive" alt="" />
					</div>
					</div>
					<div class="col-sm-6 col-md-6 col-lg-6 slogan">
						<div class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
						<h2>Signup</h2>
						<p>
						Signup and be the first one to receive news about the listing of new coins. Speed is key.
						</p>
                    </div>

                    <form class="form" method="post"> 
                        {!! csrf_field() !!}
                        <input name="email" data-wow-duration="2s" data-wow-delay="0.2s" type="email" required placeholder="Your e-mail address" class="email fadeInLeft wow" />
                        <br />
                        <input name="address" data-wow-duration="2s" data-wow-delay="0.2s" type="text" required placeholder="Your ETH or BTC wallet address where you will be paying from" class="address fadeInLeft wow" />
                        <br />
						<div class="buttons">
                            <input type="submit" value="Sign up" class="btn btn-success btn-sm wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.2s" />
                            <p class="wow fadeInLeft smally" data-wow-duration="2s" data-wow-delay="0.2s"><small>(You can either pay {{ $amount_btc }} BTC or {{ $amount_eth }} ETH)</small></p>
                        </div>
                    </form>
						
					</div>					
				</div>		
			</div>
		</div>		
    </section>
	

	<section id="contact" class="home-section paddingtop-40">	
           <div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="callaction">
							<div class="row">
								<div class="col-md-8">
									<div class="wow fadeInUp" data-wow-delay="0.1s">
									<div class="cta-text">
									<h3>You have any questions?</h3>
									<p>Feel free to reach out to us, we are happy to help.</p>
									</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="wow lightSpeedIn" data-wow-delay="0.1s">
										<div class="cta-btn">
										<a href="mailto:support@blo.re" class="btn btn-skin btn-lg">Contact us</a>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
	</section>	

	<footer>
	

		<div class="sub-footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-6 col-lg-6">
{{--
					<ul class="social">
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube"></i></a></li>
					</ul>
--}}
				
					<div class="wow fadeInLeft" data-wow-delay="0.1s">
					<div class="text-left">
					<p>&copy;Copyright 2017 - blo.re All rights reserved.</p>
					</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-6">
					<div class="wow fadeInRight" data-wow-delay="0.1s">
					</div>
				</div>
			</div>	
		</div>
		</div>
	</footer>

</div>
@include('app._footer')
