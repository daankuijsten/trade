@extends ('bot/index')

@section('content')
    <h1>Books</h1>

    <div class="order-books">
        @foreach($pairs as $pair)
            <div class="order-book">
                @include('bot._table',['pair' => $pair])
            </div>
        @endforeach
    </div>

    <div class="differences">
        <table>
            <tr>
                <th></th>
                <th>Best VIBETH</th>
                <th>Best VIBBTC</th>
                <th>Best ETHBTC</th>
                <th>Result</th>
            </tr>
            <tr class="price">
                <th class="">Price</td>
                <td class="possible" colspan="4"></td>
            </tr>
            <tr class="amount vib">
                <th class="">Amount VIB</td>
                <td class="best-vibeth"></td>
                <td class="best-vibbtc"></td>
                <td class="best-ethbtc"></td>
                <td class="result"></td>
            </tr>
            <tr class="amount eth">
                <th class="">Amount ETH</td>
                <td class="best-vibeth"></td>
                <td class="best-vibbtc"></td>
                <td class="best-ethbtc"></td>
                <td class="result"></td>
            </tr>
            <tr class="amount btc">
                <th class="">Amount BTC</td>
                <td class="best-vibeth"></td>
                <td class="best-vibbtc"></td>
                <td class="best-ethbtc"></td>
                <td class="result"></td>
            </tr>
        </table>

        <table>
            <tr>
                <th></th>
                <th>Max amount</th>
                <th>Max amount corrected</th>
            </tr>
            <tr class="max-amount vib">
                <th class="">VIB</td>
                <td class="max"></td>
                <td class="max-correct"></td>
            </tr>
            <tr class="max-amount eth">
                <th class="">ETH</td>
                <td class="max"></td>
                <td class="max-correct"></td>
            </tr>
            <tr class="max-amount btc">
                <th class="">BTC</td>
                <td class="max"></td>
                <td class="max-correct"></td>
            </tr>
        </table>
    </div>

@endsection
