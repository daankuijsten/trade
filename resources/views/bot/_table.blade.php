<h2 class="title">{{ $pair }}</h2>
<div data-pair-lower="{{ strtolower($pair) }}" data-pair-upper="{{ strtoupper($pair) }}" class="{{ strtolower($pair) }}">
    <div class="asks-container">
        <table class="asks">
            <tbody>
            </tbody>
        </table>
    </div>
    <hr>
    <div class="bids-container">
        <table class="bids">
            <tbody>
            </tbody>
        </table>
    </div>
</div>
