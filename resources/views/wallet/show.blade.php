<div class="stats {{ strtolower($currency1) }} pair-{{ strtolower($currency1) }}-{{ strtolower($currency2) }}">
    <script type="text/javascript" src="https://files.coinmarketcap.com/static/widget/currency.js"></script><div class="coinmarketcap-currency-widget" data-currency="{{ strtolower($currency1Name) }}" data-base="{{ strtolower($currency2) }}" data-secondary="USD" data-ticker="true" data-rank="true" data-marketcap="true" data-volume="true" data-stats="USD" data-statsticker="false"></div> 

    <div class="calc">
        <div class="stack">Stack: <span class="amount">{{ $currency1Amount }}</span></div>

        <table>
            <tr>
                <td class="corner"></td>
                <th>Start</th>
                <th>Current</th>
                <th>Difference</th>
            </tr>
            <tr class="{{ strtolower($currency2) }}" data-currency="{{ strtolower($currency2) }}">
                <th>{{ strtoupper($currency2) }}</th><td class="start">{{ $currency2Amount }}</td><td class="current"></td><td class="diff"></td>
            </tr>
            @if ($currency3)
                <tr class="{{ strtolower($currency3) }}" data-currency="{{ strtolower($currency3) }}">
                    <th>{{ strtoupper($currency3) }}</th><td class="start">{{ $currency3Amount }}</td><td class="current"></td><td class="diff"></td>
                </tr>
            @endif
        </table>
    </div>
</div>
