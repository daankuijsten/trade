<h1>Add wallet</h1>
<form class="add-wallet">
    <fieldset>
        <label>
            How many and what did you buy?
            <input placeholder="Amount" type="text" name="c1a" />
            <select name="c1" class="currencies">
                <optgroup class="crypto" label="Crypto currencies">
                </optgroup>
            </select>
        </label>
        <label>
        </label>
    </fieldset>

    <fieldset>
        <label>
            How many and what did you sell?
            <input placeholder="Amount" type="text" name="c2a" />
            <select name="c2" class="currencies">
                <optgroup class="fiat" label="Fiat currencies">
                    <option value="USD">USD</option> 
                    <option value="KRW">KRW</option> 
                    <option value="GBP">GBP</option> 
                    <option value="AUD">AUD</option> 
                    <option value="IDR">IDR</option> 
                    <option value="CAD">CAD</option> 
                    <option value="INR">INR</option> 
                    <option value="JPY">JPY</option> 
                    <option value="BRL">BRL</option> 
                    <option value="EUR">EUR</option> 
                    <option value="CHF">CHF</option> 
                </optgroup>
                <optgroup class="crypto" label="Crypto currencies">
                </optgroup>
             </select>
        </label>
        <label>
        </label>
    </fieldset>

{{--
    <fieldset>
        <label>
            Extra display USD or BTC?
            <select name="extra_display" class="currencies">
                <option value="">No</option>
                <option value="usd">USD</option>
                <option value="btc">BTC</option>
            </select>
        </label>
    </fieldset>

--}}
{{--
    <fieldset>
        <label>
            Do you want to add another amount and currency for this trade (for example, you sold 10 ETH which was also worth back then 3000 USD)?
            <input type="text" name="c3a" />
            <select name="c3" class="currencies">
                <optgroup class="fiat" label="Fiat currencies">
                    <option value=""></option> 
                    <option value="USD">USD</option> 
                    <option value="EUR">EUR</option> 
                    <option value="KRW">KRW</option> 
                    <option value="GBP">GBP</option> 
                    <option value="AUD">AUD</option> 
                    <option value="IDR">IDR</option> 
                    <option value="CAD">CAD</option> 
                    <option value="INR">INR</option> 
                    <option value="JPY">JPY</option> 
                    <option value="BRL">BRL</option> 
                    <option value="CHF">CHF</option> 
                </optgroup>
                <optgroup class="crypto" label="Crypto currencies">
                </optgroup>
             </select>
        </label>
        <label>
        </label>
    </fieldset>

--}}
    <input type="submit" value="Add wallet" />

</form>

