@extends ('wallet/index')

@section('content')
    <h1>Keep track of your crypto investments</h1>

    @if ($haveWallets)
        <div class="link">
            Save or bookmark this link to access your wallets again: <a href="{{ $url }}">link to wallet</a>.
        </div>
    @endif

    @include('wallet.add')

    @if ($haveWallets)
        <h1>Totals</h1>

        <div class="totals">
            <table>
                <tr>
                    <td class="corner"></td>
                    <th>Start</th>
                    <th>Current</th>
                    <th>Difference</th>
                </tr>
            </table>
        </div>

        <h1>Wallets</h1>
        
        <div class="wallets">
        <div>
    @endif

@endsection
