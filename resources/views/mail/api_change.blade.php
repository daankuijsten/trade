Dear blo.re member,<br><br>
There is a change in an API found.<br><br>
Trade pair name: <?= htmlentities($pairName) ?><br>
Trade pair active: <?= $pairActive ? 'Yes' : 'No' ?><br>
Trade pair created on: <?= $pairCreatedOnExchange ?><br>
<br><br>
This is an automated message from blo.re
