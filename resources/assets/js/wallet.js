var App = App || {};

App.Wallet = {
    setCurrency: function(currency1, currency2) {
        if (!currency1 || !currency2) {
            return false;
        }

        var stats = $('.stats.pair-' + currency1.toLowerCase() + '-' + currency2.toLowerCase());

        var value = stats.find('.coinmarketcap-currency-widget > div > div > div > span + br + span').text();
        // remove any "," in the price
        value = value.replace(',', '');

        var pattern = new RegExp('^([0-9.]+) ' + currency2.toUpperCase());
        var currency1Price = value.match(pattern);

        if (!currency1Price) {
            console.log('No currency1 found');
            return false;
        }

        if (!currency1Price[1]) {
            console.log('No currency1 found');
            loaded.resolve();
            return false;
        }
         
        currency1Price = parseFloat(currency1Price[1]);

        var currency2Amount = parseFloat(stats.find('.' + currency2.toLowerCase() + ' .start').text());
        var currency1Amount = parseFloat(stats.find('.stack .amount').text());

        stats.find('.' + currency2.toLowerCase() + ' .current').text(App.Wallet.round(currency1Amount * currency1Price, 8));

        return true;
    },
    setTotals: function() {
        var totals = {};
        $('.calc [data-currency]').each(function() {
            var currency = $(this).data('currency');
            if (!totals[currency]) {
                totals[currency] = {};
                totals[currency].start = 0;
                totals[currency].current = 0;
            }
            totals[currency].start += parseFloat($(this).find('.start').text());
            totals[currency].current += parseFloat($(this).find('.current').text());
        });

        for (key in totals) {
            var html = '<tr class="row">';
            html += '<th>' + key + '</th>';
            html += '<td class="start">' + totals[key].start + '</td>';
            html += '<td class="current">' + totals[key].current + '</td>';
            html += '<td class="diff"></td>';
            html += '</tr>';
            $('.totals table tbody').append(html);
        }

        $('.totals .row').each(function() {
            var start = parseFloat($(this).find('.start').text());
            var current = parseFloat($(this).find('.current').text());
            var diff = current / start * 100 - 100;
            $(this).find('.diff').text(App.Wallet.round(diff, 2) + '%');
            if (diff < 0) {
                $(this).find('.diff').addClass('negative');
            }
        });
    },
    calculateDiff: function() {
        $('.calc .diff').each(function() {
            var currency = $(this).closest('tr').data('currency');
            var start = $(this).closest('tr').find('.start').text();
            var current = $(this).closest('tr').find('.current').text();
            var diff = current / start * 100 - 100;
            $(this).text(App.Wallet.round(diff, 2) + '%');
            if (diff < 0) {
                $(this).addClass('negative');
            }

        });
    },
    round: function(number, decimals) {
        if (!decimals) {
            decimals = 2;
        }

        return Math.round(number * Math.pow(10,decimals)) / (Math.pow(10,decimals));
    },
    getCurrencies: function(cb) {
        var url = App.General.getBaseUrl() + 'api/get-currencies';

        axios.get(url)
        .then(function(response) {
            cb(response.data);
        })
        .catch(function(error) {
            App.General.handleResponse(error);
            App.General.loadingStop();
        });
    },
    currenciesToHTML: function(currencies) {
        var html = '';
        currencies.forEach(function(object) {
            var symbol = object.symbol.toLowerCase();
            html += '<option value="' + symbol + '">' + object.symbol + '</option>';
        });
        return html;
    },
    addWalletToQueryString: function(queryString, cb) {
        // Check if there is a ? in the current url
        var haveQuery = App.General.getQueryString();
        if (!haveQuery) {
            url = window.location.href + '?' + queryString;
        } else {
            url = window.location.href + '&' + queryString;
        }
        cb(url);
    },
    addWallet: function(data, cb) {
        var url = App.General.getBaseUrl() + 'api/get-wallet-template';

        axios.get(url + '?' + data)
        .then(function(response) {
            cb(response.data);
        })
        .catch(function(error) {
            App.General.handleResponse(error);
            App.General.loadingStop();
        });
    },
    addWalletNumber: function(queryArray) {
        var number = App.Wallet.lastWalletNumber();

        queryArray.forEach(function(object, key) {
            object.name = 'w' + (number + 1) + object.name;
        });

        return queryArray;
    },
    lastWalletNumber: function() {
        var data = App.General.getQueryString();
        if (!data) {
            return 0;
        }

        var number = 1;

        // Search for the highest w.c1 instance
        for (var key in data) {
            if (!data.hasOwnProperty(key)) {
                return;
            }
            var match = false;
            if (match = key.match(/^w([0-9]+)c1/)) {
                if (number < match[1]) {
                    number = parseInt(match[1], 10);
                }
            }
        }

        return number;
    },
    readWallets: function(widgetsLoaded) {
        var data = App.General.getQueryString();
        var walletsAdded = {};
        var currencies = [];

        var next = true;
        var i = 0;
        var count = 0;
        do {
            i++;
            if (!data) {
                next = false;
                break;
            }

            var name1 = 'w' + i + 'c1';
            var name2 = 'w' + i + 'c2';
            var name3 = 'w' + i + 'c3';
            var name1Amount = 'w' + i + 'c1a';
            var name2Amount = 'w' + i + 'c2a';
            var name3Amount = 'w' + i + 'c3a';

            if (!data[name1]) {
                next = false;
                break;
            }

            if (!data[name1Amount]) {
                next = false;
                break;
            }

            if (!data[name2]) {
                next = false;
                break;
            }

            if (!data[name2Amount]) {
                next = false;
                break;
            }

            if (!data[name3]) {
                data[name3] = '';
            }

            if (!data[name3Amount]) {
                data[name3Amount] = '';
            }

            // Make a query, strip the wallet number
            var query = name1.substr(-2) + '=' + encodeURIComponent(data[name1]);
            query += '&' + name2.substr(-2) + '=' + encodeURIComponent(data[name2]);
            query += '&' + name3.substr(-2) + '=' + encodeURIComponent(data[name3]);
            query += '&' + name1Amount.substr(-3) + '=' + encodeURIComponent(data[name1Amount]);
            query += '&' + name2Amount.substr(-3) + '=' + encodeURIComponent(data[name2Amount]);
            query += '&' + name3Amount.substr(-3) + '=' + encodeURIComponent(data[name3Amount]);

            walletsAdded[i] = $.Deferred();
            currencies[i] = { 
                currency1: data[name1],
                currency2: data[name2],
            };

            App.Wallet.addWallet(query, function(template) {
                count++;
                $('.wallets').append(template);
                walletsAdded[count].resolve(currencies[count]);
            });

        } while (next);
        
        return walletsAdded;

    },
    widgetsLoaded: function() {
        var widgetsLoaded = $.Deferred();
        var hasValue = false;

        // Check if ALL widgets are loaded
        // Check if the widget is there and for each widget if the price is set
        // Sometimes the price is later then the widget itself
        var checkLoaded = setInterval(function() {
            count = 0;
            $('.coinmarketcap-currency-widget').each(function() {
                if ($(this).find('> div > div > div > span + br + span').length) {
                    count++;
                }
            });

            if (count === App.Wallet.lastWalletNumber()) {
                widgetsLoaded.resolve();
                clearInterval(checkLoaded);
            }
        }, 100);

        return widgetsLoaded;
    },
    validateInput: function() {
        var currency1 = $('[name=c1]').val();
        var currency1Amount = $('[name=c1a]').val();
        var currency2 = $('[name=c2]').val();
        var currency2Amount = $('[name=c2a]').val();

        if (!currency1Amount || !currency2Amount) {
            App.General.setMessages({ 
                error: [['Fill in both amounts']]
            });
            return false;
        }

        var pattern = new RegExp('[^0-9.]+');
        if (pattern.test(currency1Amount) || pattern.test(currency2Amount)) {
            App.General.setMessages({ 
                error: [['Use only numbers and a period "." for the amount']]
            });
            return false;
        }

        if (currency1 === currency2) {
            App.General.setMessages({ 
                error: [['Currencies must be different']]
            });
            return false;
        }
                    

        return true;
    }

}

$(function() {
    var widgetsLoaded = App.Wallet.widgetsLoaded();

    var html = '';
    App.Wallet.getCurrencies(function(currencies) {
        html = App.Wallet.currenciesToHTML(currencies);
        $('.currencies optgroup.crypto').append(html);
    });

    $('.add-wallet').submit(function(e) {
        e.preventDefault();

        if (!App.Wallet.validateInput()) {
            return false;
        }

        var data = App.Wallet.addWalletNumber($(this).serializeArray());
        data = jQuery.param(data);

        App.Wallet.addWalletToQueryString(data, function(url) {
            window.location.replace(url);
        });
    });

    var walletsLoaded = App.Wallet.readWallets(widgetsLoaded);
    var allSet = $.Deferred();

    // When the widget is loaded,
    // Start loading the wallets
    // Set the current amount
    // Set the diff
    var count = 0;
    $.when(widgetsLoaded).then(function() {
        $.when(walletsLoaded).done(function(currencies) {

            var total = Object.keys(currencies).length;
            for (var key in currencies) {
                currencies[key].done(function(currency) {
                    App.Wallet.setCurrency(currency.currency1, currency.currency2);

                    count++;
                    if (count === total) {
                        allSet.resolve();
                    }
                });
            }
        });
    });

    $.when(allSet).done(function() {
        App.Wallet.calculateDiff();
        App.Wallet.setTotals();
    });
});

