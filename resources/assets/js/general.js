var App = App || {};

App.General = {
    confirmWarning: function() {
        if (confirm('Are you sure?')) {
            return true;
        }
        return false;
    },
    handleCloseMessage: function() {
        $('#messages').on('click', function() {
            App.General.clearMessages();
        });
    },
    clearMessages: function() {
        $('#messages > *').remove();
    },
    setMessages: function(messages) {
        if (!messages) {
            return false;
        }

        var messagesAll;

        for(type of ['error', 'success']) {
            if (!messages[type] || (messages[type] !== Object(messages[type]) && !messages[type].length)) {
                continue;
            }

            // Transform objects in arrays, ignore key
            if (messages[type] === Object(messages[type])) {
                messages[type] = messages[type][Object.keys(messages[type])[0]];
            }

            // Could be undefined after transformation
            if (!messages[type]) {
                continue;
            }

            messages[type].forEach(function(message) {
                $('#messages').append('<p class="alert alert-' +  type + '">' + message + '<span class="close ion-android-close"></span></p>');
            });
        }

        App.General.handleCloseMessage();

    },
    handleResponse: function(error) {
        if (error.response) {
            if (error.response.data && error.response.data.messages) {
                App.General.setMessages(error.response.data.messages);
            }
        } else if (error.request) {
          // The request was made but no response was received
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          console.log(error.request);
        } else {
            // Something happened in setting up the request that triggered an Error
            console.log(error);
        }
    },
    loadingStart: function(selector) {
        if (!selector) {
            selector = $('body');
        }

        App.General.loadingStop(selector);

        var loading = $('#loading-parent').clone().prop('id', 'loading');
        selector.append(loading);
        loading.fadeIn(300)

    },
    loadingStop: function(selector) {
        if (!selector) {
            selector = $('body');
        }

        selector.find('#loading').fadeOut(300, function() {
            $(this).remove();
        });
    },
    handleCheckAll: function() {
        $('.check-all-control').on('click', function() {
            var active = $(this).prop('checked');

            $('input.check-all-active').each(function() {
                $(this).prop('checked', active);
            });
        });
    },
    handleTableSorter: function() {
        if ($("table.normalize.sortable").length) {
            $.each($("table.normalize.sortable"), function() {
                // updateAll is needed for when table are filled with AJAX
                $(this).tablesorter().trigger('updateAll');
            });
        }
    },
    getBaseUrl: function() {
        if (window.location.protocol != 'https') {
            return location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/';
        } else {
            return location.protocol + '//' + window.location.hostname + '/';
        }
    },
    getCurrentUrl: function() {
        return window.location.href;
    },
    getQueryString: function (queryString) {
        var match,
            pl     = /\+/g,  // Regex for replacing addition symbol with a space
            search = /([^&=]+)=?([^&]*)/g,
            decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
            query  = window.location.search.substring(1);

        if (queryString) {
            query = queryString;
        }

        urlParams = {};
        while (match = search.exec(query))
           urlParams[decode(match[1])] = decode(match[2]);

        if (jQuery.isEmptyObject(urlParams)) {
            return false;
        }

        return urlParams;
    }
}

$(function() {
    App.General.handleCloseMessage();
    App.General.handleCheckAll();
    App.General.handleTableSorter();

    // navigation slide open/close
    $('nav .toggle > a').on('click', function(e) {
        e.preventDefault();
         
        $(this).closest('li').find('.child').slideToggle();
        $(this).closest('li').toggleClass('open');
    });

    $('a.confirm').on('click', function() {
        return App.General.confirmWarning();
    });
});

