var App = App || {};

App.App = {
    getPairs: function(url, cb) {
        App.General.clearMessages();
        App.General.loadingStart();

        var url = 'http://daan.localhost:8035/api/get-pairs/liq'
        axios.get(url)
        .then(function(response) {
            App.General.loadingStop();
            cb(response.data);
        })
        .catch(function(error) {
            App.General.handleResponse(error);
            App.General.loadingStop();
        });
    },
    createGraph: function(url) {
        // set the dimensions and margins of the graph
        var margin = {top: 20, right: 20, bottom: 30, left: 50},
            width = 1200 - margin.left - margin.right,
            height = 600 - margin.top - margin.bottom;

        // parse the date / time
        var parseTime = d3.timeParse("%d-%b-%y");

        // set the ranges
        var x = d3.scaleTime()
            .range([0, width])
            .clamp(true);

        var y = d3.scaleLinear().range([height, 0]);

        // define the line
        var valueline = d3.line()
            .x(function(d) { return x(d.date); })
            .y(function(d) { return y(d.price); });

        // append the svg obgect to the body of the page
        // appends a 'group' element to 'svg'
        // moves the 'group' element to the top left margin
        var svg = d3.select("body").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
          .append("g")
            .attr("transform",
                  "translate(" + margin.left + "," + margin.top + ")");

        console.log(url);
        // Get the data
        d3.json(url, function(error, data) {
          if (error) throw error;

          // format the data
          data.forEach(function(d) {
              d.date = new Date(d.timestamp);
          });

          // Scale the range of the data
          x.domain(d3.extent(data, function(d) { return d.date; }));
          y.domain([d3.min(data, function(d) { return d.price; }), d3.max(data, function(d) { return d.price; })]);

          // Add the valueline path.
          svg.append("path")
              .data([data])
              .attr("class", "line")
              .attr("d", valueline);

          // Add the X Axis
          svg.append("g")
              .attr("transform", "translate(0," + height + ")")
              .call(d3.axisBottom(x));

          // Add the Y Axis
          svg.append("g")
              .call(d3.axisLeft(y));

          App.App.createDataBlock(data);

        });
    },
    createDataBlock: function(trades) {
        dayChange = App.App.calculate24HourChange(trades);

        $.each(data, function(key, trade) {
            //console.log(dayChange);
        });
    },
    calculate24HourChange(trades) {

        $.each(trades.reverse(), function(key, trade) {
            dateObj = new Date;
                console.log(dateObj.setDate(dateObj.getTime()-(60*60*24)));
            if (trade.date < dateObj.setDate(dateObj.getTime()-(60*60*24))) {

            }
        });
    }
}

$(function() {
    App.App.getPairs('http://daan.dev:8035/api/get-pairs/liq', function(pairs) {
        var i = 0;
        $.each(pairs, function(key, pair) {
            if (i > 1) {
                return false;
            }
            i++;
            App.App.createGraph('http://daan.dev:8035/api/get-trades/liq/' + pair.name);
        });
    });
});
