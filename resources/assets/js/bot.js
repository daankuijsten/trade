var App = App || {};

App.Bot = {
/*
    buildAsks: function(socket) {
        if (!socket.length) {
            throw new Exception('socket empty');
        }

        // asks
        socket.a


         
    },
*/
}

$(function() {
    var binance = BinanceApi;

// Periods: 1m,3m,5m,15m,30m,1h,2h,4h,6h,8h,12h,1d,3d,1w,1M
/*
binance.websockets.candlesticks(['BNBBTC'], "1m", function(candlesticks) {
    let { e:eventType, E:eventTime, s:symbol, k:ticks } = candlesticks;
    let { o:open, h:high, l:low, c:close, v:volume, n:trades, i:interval, x:isFinal, q:quoteVolume, V:buyVolume, Q:quoteBuyVolume } = ticks;
    console.log(symbol+" "+interval+" candlestick update");
    console.log("open: "+open);
    console.log("high: "+high);
    console.log("low: "+low);
    console.log("close: "+close);
    console.log("volume: "+volume);
    console.log("isFinal: "+isFinal);
});
    */
// Periods: 1m,3m,5m,15m,30m,1h,2h,4h,6h,8h,12h,1d,3d,1w,1M
//binance.websockets.depth(['VIBETH'], function(data) {
//console.log(data);
/*
    let { e:eventType, E:eventTime, s:symbol, k:ticks } = candlesticks;
    let { o:open, h:high, l:low, c:close, v:volume, n:trades, i:interval, x:isFinal, q:quoteVolume, V:buyVolume, Q:quoteBuyVolume } = ticks;
    console.log(symbol+" "+interval+" candlestick update");
    console.log("open: "+open);
    console.log("high: "+high);
    console.log("low: "+low);
    console.log("close: "+close);
    console.log("volume: "+volume);
    console.log("isFinal: "+isFinal);
*/
//});

$('[data-pair-lower]').each(function() {
    var pairLower = $(this).data('pair-lower');
    var pairUpper = $(this).data('pair-upper');
    console.log('neeee');

    var k = 0;
    binance.websockets.depthCache([pairUpper], function(symbol, depth) {
        k++;
        if (k > 2) {
            //return;
        }
        let bids = binance.sortBids(depth.bids);
        let asks = binance.sortAsks(depth.asks);

        $('.' + pairLower).find('.order').remove();

        var j = Object.keys(asks).length;
        for (var key in asks) {
            var tr = '<tr class="order number' + j + '"><td class="price">' + key + '</td><td class="amount">' + asks[key] + '</td></tr>';
            $('.' + pairLower).find('.asks tbody').append(tr);
            j--;
        }
        var i = 0;
        for (var key in bids) {
            i++;
            var tr = '<tr class="order number' + i + '"><td class="price">' + key + '</td><td class="amount">' + bids[key] + '</td></tr>';
            $('.' + pairLower).find('.bids tbody').append(tr);
        }
        //console.log(symbol+" depth cache update");

        getDifferences(pairLower);
    }, 20);

});

function splitPair(pair) {
    return [pair.substr(0,3), pair.substr(-3)];
}

function getDifferences(pair) {
    //var vibEthPrice = parseFloat($('.vibeth .asks .number1 .price').text());
    //var vibEthAmount = parseFloat($('.vibeth .asks .number1 .amount').text());
    var vibEthAmountEth = vibEthAmount * vibEthPrice;
    var vibEthPrice = parseFloat($('.trxeth .asks .number1 .price').text());
    var vibEthAmount = parseFloat($('.trxeth .asks .number1 .amount').text());

    //var vibBtcPrice = parseFloat($('.vibbtc .bids .number1 .price').text());
    //var vibBtcAmount = parseFloat($('.vibbtc .bids .number1 .amount').text());
    var vibBtcPrice = parseFloat($('.trxbtc .bids .number1 .price').text());
    var vibBtcAmount = parseFloat($('.trxbtc .bids .number1 .amount').text());
    var vibBtcAmountBtc = vibBtcAmount * vibBtcPrice;
    //console.log(vibBtcAmount, vibBtcPrice, vibBtcAmountBtc, round(vibBtcAmountBtc, 10));

    var ethBtcPrice = parseFloat($('.ethbtc .asks .number1 .price').text());
    var ethBtcAmount = parseFloat($('.ethbtc .asks .number1 .amount').text());
    var ethBtcAmountEth = ethBtcAmount * ethBtcPrice;

    var possible = (vibBtcPrice / ethBtcPrice - vibEthPrice);
    console.log(possible);

    $('.amount.vib .best-vibeth').text(round(vibEthAmount, 1));
    $('.amount.eth .best-vibeth').text(round(vibEthAmountEth, 6));

    $('.amount.vib .best-vibbtc').text(round(vibBtcAmount, 1));
    $('.amount.btc .best-vibbtc').text(round(vibBtcAmountBtc, 6));

    $('.amount.btc .best-ethbtc').text(round(ethBtcAmount, 6));
    $('.amount.eth .best-ethbtc').text(round(ethBtcAmountEth, 6));

    // Calculates lowest ratio
    /*
    var ratioVib = Math.min(vibEthAmount, vibEthAmountEth) / Math.max(vibEthAmount, vibEthAmountEth);
    var ratioEth = Math.min(vibBtcAmount, vibBtcAmountBtc) / Math.max(vibBtcAmount, vibBtcAmountBtc);
    var ratioBtc = Math.min(ethBtcAmount, ethBtcAmountEth) / Math.max(ethBtcAmount, ethBtcAmountEth);

    var minRatio = Math.min(ratioVib, ratioEth, ratioBtc);
    */

    var vibEthVibCorrect = vibEthAmount;
    var vibEthEthCorrect = vibEthAmountEth;
    var vibBtcVibCorrect = vibBtcAmount;
    var vibBtcBtcCorrect = vibBtcAmountBtc;
    var ethBtcEthCorrect = ethBtcAmountEth;
    var ethBtcBtcCorrect = ethBtcAmount;

    if (vibEthVibCorrect < vibBtcVibCorrect) {
        // VIBETH-VIB is smallest
        // correct the VIBBTC pair
        vibBtcBtcCorrect = vibBtcBtcCorrect * (vibEthVibCorrect / vibBtcVibCorrect);
        vibBtcVibCorrect = vibEthVibCorrect;
    } else {
        // VIBBTC-VIB is smallest
        // correct the VIBETH pair
        vibEthEthCorrect = vibEthEthCorrect * (vibBtcVibCorrect / vibEthVibCorrect);
        vibEthVibCorrect = vibBtcVibCorrect;
    }

    if (vibEthEthCorrect < ethBtcEthCorrect) {
        // VIBETH-ETH is smallest
        // correct the ETHBTC pair
        ethBtcBtcCorrect = ethBtcBtcCorrect * (vibEthEthCorrect / ethBtcEthCorrect);
        ethBtcEthCorrect = vibEthEthCorrect;
    } else {
        // ETHBTC-ETH is smallest
        // correct the VIBETH pair
        // correct the (previous) VIBBTC pair
        vibEthVibCorrect = vibEthVibCorrect * (ethBtcEthCorrect / vibEthEthCorrect);
        vibBtcBtcCorrect = vibBtcBtcCorrect * (ethBtcEthCorrect / vibEthEthCorrect);

        vibBtcVibCorrect = vibEthVibCorrect;
        vibEthEthCorrect = ethBtcEthCorrect;
    }

    if (vibBtcBtcCorrect < ethBtcBtcCorrect) {
        // VIBBTC-BTC is smallest
        // correct the ETHBTC pair
        // correct the (previous) VIBETH pair
        // at last correct the VIBBTC-BTC again
        ethBtcEthCorrect = ethBtcEthCorrect * (vibBtcBtcCorrect / ethBtcBtcCorrect);
        vibEthVibCorrect = vibEthVibCorrect * (vibBtcBtcCorrect / ethBtcBtcCorrect);
        vibBtcBtcCorrect = vibBtcBtcCorrect * (vibBtcBtcCorrect / ethBtcBtcCorrect);

        // last
        vibBtcVibCorrect = vibEthVibCorrect;
        vibEthEthCorrect = ethBtcEthCorrect;
        ethBtcBtcCorrect = vibBtcBtcCorrect;

        //vibBtcBtcCorrect = 

    }

    // Get the min with init/first value and VIBETH, VIB amount
    var minVibEthVib = Math.min(vibEthAmount, vibBtcAmount);

    // Correct into ETH
    var minVibEthEth = (vibEthAmountEth * (minVibEthVib / vibEthAmount));

    // Get the min with last value and ETHBTC, ETH amount
    var minEthBtcEth = Math.min(minVibEthEth, ethBtcAmountEth);

    // Correct into BTC
    var minEthBtcBtc = (ethBtcAmount * (minEthBtcEth / ethBtcAmountEth));

    // Get the min with last value and VIBBTC, BTC amount
    var minVibBtcBtc = Math.min(minEthBtcBtc, vibBtcAmountBtc);

    // Correct into VIB
    var minVibBtcVib = (vibBtcAmount * (minVibBtcBtc / vibBtcAmountBtc));


    var vibCorrected = Math.min(minVibBtcVib, vibEthAmount);
    var ethCorrected = (vibEthAmountEth * (vibCorrected / vibEthAmount));
    var btcCorrected = (vibBtcAmountBtc * (vibCorrected / vibBtcAmount));

    $('.max-amount.vib .max').text(round(minVibEthVib, 6));
    $('.max-amount.vib .max-correct').text(round(vibCorrected, 6));

    $('.max-amount.eth .max').text(round(minEthBtcEth, 6));
    $('.max-amount.eth .max-correct').text(round(ethCorrected, 6));

    $('.max-amount.btc .max').text(round(minVibBtcBtc, 6));
    $('.max-amount.btc .max-correct').text(round(btcCorrected, 6));

    // check if we can sell


    //console.log(ethCorrected > ethBtcAmountEth ? 'wrong' : 'right');
    //console.log(btcCorrected > ethBtcAmount ? 'wrong' : 'right');


    //$('.amount.vib .best-vibbtc').text(vibAmountRaw);

    /*
    var vib2Price = (1 / vib2PriceRaw);
    var vib2Amount = 1 / (vib2AmountRaw * vib2PriceRaw);

    var vibPrice = (1 / vibPriceRaw) * btcPrice;
    var vibAmount = 1 / (vibAmountRaw * vibPriceRaw);

    $('.price .best-vibeth').text(Math.round(vib2Price * 1000) / 1000);

    $('.amount.vib .best-vibeth').text(round(vib2Amount, 4));
    $('.amount.eth .best-vibeth').text(round(vib2Amount * vibEthPriceRaw, 6));

    $('.price .best-ethbtc').text(ethBtcPrice);

    $('.amount.btc .best-ethbtc').text(round(btcAmount * btcPrice, 8));
    $('.amount.eth .best-ethbtc').text(ethBtcAmount);

    $('.price .best-vibbtc').text(Math.round(vibPrice * 1000) / 1000);

    $('.amount.vib .best-vibbtc').text(vibAmountRaw);
    $('.amount.btc .best-vibbtc').text(Math.round(vibAmount * vibPriceRaw * 10000000) / 10000000);

    var result = vibPrice - vib2Price;
    $('.price .result').text(Math.round(result * 1000) / 1000);

    */


}

function round(number, decimals) {
    return Math.round(number * Math.pow(10, decimals)) / (Math.pow(10, decimals));
}


/*
binance.websockets.trades(['VIBETH', 'ETHBTC', 'VIBBTC'], function(trades) {
    let {e:eventType, E:eventTime, s:symbol, p:price, q:quantity, m:maker, a:tradeId} = trades;
    console.log(symbol+" trade update. price: "+price+", quantity: "+quantity+", maker: "+maker);
});

*/
/*
binance.prices(function(ticker) {
    console.log("prices()", ticker);
    console.log("Price of BNB: ", ticker.BNBBTC);
});
/*
    console.log('hahha');


    var webSocket = $.simpleWebSocket({ url: 'wss://stream.binance.com:9443/ws/bnbbtc@depth' });

    // reconnected listening
    webSocket.listen(function(message, a, b) {
        console.log(message);
    });

/*
    webSocket.send({ 'text': 'hello' }).done(function() {
        // message send
    }).fail(function(e) {
        // error sending
    });
*/
});

