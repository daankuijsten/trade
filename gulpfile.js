var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.scripts([
        './node_modules/axios/dist/axios.min.js',
        './node_modules/jquery/dist/jquery.min.js',
//        './node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
        //'./node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
        //'./node_modules/moment/min/moment.min.js',
        //'./node_modules/chart.js/dist/Chart.min.js',
        //'./node_modules/moment/min/moment.min.js',
        //'./node_modules/tablesorter/dist/js/jquery.tablesorter.min.js',

        'resources/assets/js/bootstrap.js', 
        'resources/assets/js/general.js', 
        'resources/assets/js/app.js', 
    ], 'public/js/app.js');

    mix.scripts([
        './node_modules/axios/dist/axios.min.js',
        './node_modules/jquery/dist/jquery.min.js',
        'resources/assets/js/bootstrap.js', 
        'resources/assets/js/general.js', 
        'resources/assets/js/wallet.js', 
    ], 'public/js/wallet.js');

    mix.scripts([
        //'./node_modules/requirejs/require.js',
        //'./node_modules/node-binance-api/node-binance-api.js',
        './node_modules/jquery/dist/jquery.min.js',
        './node_modules/axios/dist/axios.min.js',
        './node_modules/jquery-simple-websocket/dist/jquery.simple.websocket.min.js',
        //'resources/assets/js/general.js', 
        //'resources/assets/js/node-binance-api.js', 
        'resources/assets/js/binance-api.js', 
        'resources/assets/js/bot.js', 
    ], 'public/js/bot.js');


    mix.sass([
//        './node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.standalone.min.css',
        'resources/assets/sass/app/app.scss',
        'resources/assets/sass/wallet/app.scss',
    ], 'public/css');

    mix.sass([
        'resources/assets/sass/bot/app.scss',
    ], 'public/css/bot.css');

    mix.sass([
        'resources/assets/sass/wg/app.scss',
    ], 'public/css/wg.css');

    mix.browserSync({
        proxy: 'daan.localhost:8034',
        host: 'daan.localhost',
        port: 8034,
        open: false,
    });
});
